/**
 *  SoundHandler
 *
 *  Created by Justin Ng on 2014-08-19.
 *  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
 */

ig.module('plugins.audio.sound-info')
.requires(
)
.defines(function () {

    SoundInfo = ig.Class.extend({
		FORMATS:{
			OGG:".ogg",
			MP3:".mp3",
		},

		/**
		* Define your sounds here
		*
        */
		sfx:{
			kittyopeningSound:{path:"media/audio/opening/kittyopening"}
			,staticSound:{path:"media/audio/play/static"}
			,openingSound:{path:"media/audio/opening/opening"}

               ,ballLaunch:{path:"media/audio/game/balllaunch"}
               ,flipper:{path:"media/audio/game/flipper"}
               ,getGem:{path:"media/audio/game/getgem"}
               ,magnetOff:{path:"media/audio/game/magnetoff"}
               ,magnetOn:{path:"media/audio/game/magneton"}
               ,sureShot:{path:"media/audio/game/sureshot"}
               ,sureShotReady:{path:"media/audio/game/sureshotready"}
               ,hitObstacle:{path:"media/audio/game/hitobstacle"}

               // Timer
               ,beep:{path:"media/audio/game/beep"}
               ,go:{path:"media/audio/game/go"}

               // UI
               ,upgrade:{path:"media/audio/game/upgrade"}
               ,click:{path:"media/audio/game/click"}
               ,gameOver:{path:"media/audio/game/gameover"}
               //,transition:{path:"media/audio/game/whoosh"}
		},

        /**
        * Define your BGM here
        */
		bgm:{
			background:{path:'media/audio/bgm',startOgg:0,endOgg:18.847,startMp3:0,endMp3:18.834}
		}


    });

});
