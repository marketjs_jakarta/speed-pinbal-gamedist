ig.module('plugins.splash-loader')
.requires(
    'impact.loader',
    'impact.animation'
)
.defines(function() {
    ig.SplashLoader = ig.Loader.extend({
        //splashDesktop: new ig.Image('media/graphics/splash/desktop/cover.png'),
        splashMobile: new ig.Image('media/graphics/splash/mobile/cover.png'),

        //customAnim: new ig.AnimationSheet('media/graphics/splash/loading/anim.png',256,160),//Use your own file

        barBase: new ig.Image('media/graphics/splash/loading/barbase.png'),
        barLoading: new ig.Image('media/graphics/splash/loading/barloading.png'),
        barSize: {x:405, y:22},

        init:function(gameClass,resources){

            this.parent(gameClass,resources);
            //console.log("asdasdasd");
            // ENABLE, IF CUSTOM ANIMATION REQUIRED DURING LOADING
            //this.setupCustomAnimation();

            // ADS
            ig.apiHandler.run("MJSPreroll");
        },

        end:function(){
            this.parent();

            if(ig.ua.mobile)
            {
                var play = ig.domHandler.getElementById("#play");
                ig.domHandler.show(play);
            }

            ig.system.setGame(MyGame);

            // CLEAR CUSTOM ANIMATION TIMER
            // window.clearInterval(ig.loadingScreen.animationTimer);
        },

        setupCustomAnimation:function(){
            this.animHeight = this.customAnim.height;
            this.animWidth = this.customAnim.width;
            this.customAnim = new ig.Animation(this.customAnim, 0.025, [0,1,2,3,4,5,6,7]);
            // this.customAnim.currentFrame = 0;

            // // Assign this class instance an arbitrary name
            // ig.loadingScreen = this;

            // // Create an external timer variable
            // ig.loadingScreen.animationTimer = window.setInterval('ig.loadingScreen.animate()',100);
        },

          animate:function(){
               // Somehow the update() function doesn't work in Loader class. Resort to using external timer to increment
               // current frame in anim object

               // if(this.customAnim.currentFrame<this.customAnim.sequence.length){
               //     this.customAnim.currentFrame++;
               // }else{
               //     this.customAnim.currentFrame=0;
               // }
               // this.customAnim.gotoFrame(this.customAnim.currentFrame);
               ig.Timer.step();
               this.customAnim.update();
          },

          draw: function() {

               this._drawStatus += (this.status - this._drawStatus)/5;

               // CLEAR RECTANGLE
               ig.system.context.fillStyle = '#000';
               ig.system.context.fillRect( 0, 0, ig.system.width, ig.system.height );

               var s = ig.system.scale;

               // DIMENSIONS OF LOADING BAR
               var w,h,x,y;
               w = 180;
               h = 24;
               x = 64;
               y = 552;
               this.splashMobile.draw(0,0);

               // Loading bar
               this.barBase.draw(63, 554);
               var     loadPosX = 67, loadPosY = 557,
                    widthBar = this.barSize.x*this._drawStatus,
                    scale = ig.system.scale;
               this.barLoading.draw(loadPosX, loadPosY, 0, 0, widthBar * scale, this.barSize.y);

               // preload fonts
               ig.system.context.font = "45px Nord"; // set font
               ig.system.context.fillStyle = '#aed6f3';
               ig.system.context.fillText('LOADING', 200, 650 );

               ig.system.context.font = "1px Somatic Rounded"; // set font
               ig.system.context.fillText('asdasd', -100, -100 ); // draw offscreen
          }
    });
});
