ig.module("plugins.gamedist").defines(function() {

    /* window.GD_OPTIONS = {
        gameId: "782d285ae87c4d51bf4720a71184b9e0",
        advertisementSettings: {
            debug: !1,
            autoplay: !1,
            locale: "en"
        },
        onEvent: function(b) {
            switch (b.name) {
            case "SDK_GAME_START":
                ig.gd.unfreez();
                if(typeof(ig.gd.onAdClose) === 'function'){
					ig.gd.onAdClose();
				}  
                ig.gd.onAdClose = 0;
                break;
            case "SDK_GAME_PAUSE":
                ig.gd.freez();
            }
        }
    };
    var b, c = document.getElementsByTagName("script")[0];
    document.getElementById("gamedistribution-jssdk") || (b = document.createElement("script"),
    b.id = "gamedistribution-jssdk",
    b.src = "https://html5.api.gamedistribution.com/main.min.js",
    c.parentNode.insertBefore(b, c)); */

    ig.Gd = ig.Class.extend({
        pausedMidplay: 0,
        prevMuted: {
            bgm: 0,
            sfx: 0
        },
        onAdClose: 0,
        adShowing: 0,
        isFreez: 0,
        createTestButton:function(){
            this.createDiv('Resume', 5, 'green', 'white', function(){
                if(ig.game && ig.gd){ig.gd.unfreez();}
            });
            this.createDiv('Pause', 70, 'red', 'white', function(){
                if(ig.game && ig.gd){ig.gd.freez();}
            });
        },
        createDiv: function(caption, left, bgcolor, color, onClick){
            var button = ig.$new('button');
            document.body.appendChild(button);
            button.textContent = caption;
            button.style.position = 'absolute';
            button.style.color = color;
            button.style.background =  bgcolor;
            button.style.top = '5px';
            button.style.left = left+'px';
            button.style.padding = '5px';
            button.style.fontSize = '12px';
            button.style.fontFamily = 'Arial';
            button.style.cursor = 'pointer';
            button.style['z-index'] = 999999;
            button.onclick = onClick;
        },
        freez: function() {
            if(!gdsdk) return;

            if(ig.game){
				//Mute / unmute the sound based on previous state
				
				this.prevMuted.bgm=ig.soundHandler.bgmPlayer.mute();
				this.prevMuted.sfx = ig.soundHandler.sfxPlayer.mute();
				
				//Pause / unpause the game based on previous state
				ig.game.pauseGame();
			}
            this.isFreez = 1;
        },
        unfreez: function() {
            if(!gdsdk) return;
            this.adShowing = 0;
            this.isFreez = 0;
            
            if(ig.game){
				//Mute / unmute the sound based on previous state
				
				this.prevMuted.bgm=ig.soundHandler.bgmPlayer.unmute();
				this.prevMuted.sfx = ig.soundHandler.sfxPlayer.unmute();
				
				//Pause / unpause the game based on previous state
				ig.game.resumeGame();
			}
        },
        show: function(onClose) {
            
            if(gdsdk){
				console.log('Mid roll. Play button');
                if(this.adShowing) return;
                this.adShowing = 1;
                this.onAdClose = onClose || 0;
                gdsdk.showBanner();
            }else{
                onClose();
            };
        }
    });
    ig.gd = new ig.Gd();
});