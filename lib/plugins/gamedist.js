//Gamedistribution V 1.2.0
ig.module("plugins.gamedist").defines(function() {

    window.GD_OPTIONS = {
        gameId: "782d285ae87c4d51bf4720a71184b9e0",//Change with your gameId
        advertisementSettings: {
            debug: !1,
            autoplay: !1,
            locale: "en"
        },
        onEvent: function(b) {
            switch (b.name) {
                case "SDK_READY":
                    console.log('Mjs-Gd: "The sdk is ready"');
                    ig.gd.ready = true;
                    gameStart();
                break;
                case "SDK_ERROR":
                    console.log('Mjs-Gd: "The sdk is error"');
                    gameStart();
                break;
                case "SDK_GAME_START":
                    ig.gd.unfreez();
                    if(typeof ig.gd.onAdClose === 'function')
                        ig.gd.onAdClose();
                    ig.gd.onAdClose = 0;
                break;
                case "SDK_GAME_PAUSE":
                    ig.gd.freez();
                break;
            }
        }
    };
    (function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s);
        js.id = id;
        js.src = 'https://html5.api.gamedistribution.com/main.min.js';
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'gamedistribution-jssdk'));

    ig.Gd = ig.Class.extend({
        pausedMidplay: 0,
        prevMuted: {
            bgm: 0,
            sfx: 0
        },
        onAdClose: 0,
        adShowing: 0,
        isFreez: 0,
        ready: false,
        createTestButton: function(){
            this.createDiv('Resume', 5, 'green', 'white', function(){
                if(ig.game && ig.gd){ig.gd.unfreez();}
            });
            this.createDiv('Pause', 70, 'red', 'white', function(){
                if(ig.game && ig.gd){ig.gd.freez();}
            });
            this.createDiv('Show Rewarded', 124, 'purple', 'white', function(){
                if(ig.game && ig.gd){ig.gd.showRewarded();}
            });
        },
        createDiv: function(caption, left, bgcolor, color, onClick){
            var button = ig.$new('button');
            document.body.appendChild(button);
            button.textContent = caption;
            button.style.position = 'absolute';
            button.style.color = color;
            button.style.background =  bgcolor;
            button.style.top = '5px';
            button.style.left = left+'px';
            button.style.padding = '5px';
            button.style.fontSize = '12px';
            button.style.fontFamily = 'Arial';
            button.style.cursor = 'pointer';
            button.style['z-index'] = 999999;
            button.onclick = onClick;
        },
        freez: function() {
            if(typeof gdsdk === 'undefined') return;

            //TODO:[Gamdist]-----------------
            //Grab current sound mute / unmute state.
            //Save it into custom variable. e.g prevMuted. 
            //Then mute the sounds
            if(ig.game){
				//Mute / unmute the sound based on previous state
				
				this.prevMuted.bgm=ig.soundHandler.bgmPlayer.mute();
				this.prevMuted.sfx = ig.soundHandler.sfxPlayer.mute();
				
			}

            //Pause the game
            if(ig.game) ig.game.pauseGame();
            ig.gd.isFreez = 1;
        },
        unfreez: function() {
            if(typeof gdsdk === 'undefined') return;
            ig.gd.adShowing = 0;
            ig.gd.isFreez = 0;
            
            //TODO:[Gamdist]----------------
            //Mute / unmute the sound based on previous state
            if(ig.game){
				//Mute / unmute the sound based on previous state
				
				this.prevMuted.bgm=ig.soundHandler.bgmPlayer.unmute();
				this.prevMuted.sfx = ig.soundHandler.sfxPlayer.unmute();
				
			}

            //Resume the game
            if(ig.game) ig.game.resumeGame();
        },
        show: function(onClose) {
            if(typeof gdsdk !== 'undefined' && gdsdk.showAd !== 'undefined' && gdsdk.preloadAd !== 'undefined' && ig.gd.ready){
                if(ig.gd.adShowing) return;
                gdsdk
                .preloadAd()
                .then(function(response){
                    console.log('Mjs-Gd: "Ad is avaiable - Showing ad"');
                    ig.gd.adShowing = 1;
                    ig.gd.onAdClose = onClose || 0;
                    gdsdk.showAd().catch(function(err){console.log('Mjs-Gd: '+err); if(typeof onClose === 'function') onClose(); ig.gd.unfreez();});
                })
                .catch(function(error){
                    console.log('Mjs-Gd: "Ad is not avaiable"')
                    if(typeof onClose === 'function')
                        onClose();
                });
            }else{  
                onClose();
            };
        },
        showRewarded: function(onClose) {
            if(typeof gdsdk !== 'undefined' && gdsdk.showAd !== 'undefined'){
                if(ig.gd.adShowing) return;
                ig.gd.adShowing = 1;
                ig.gd.onAdClose = onClose || 0;
                gdsdk.showAd('rewarded').catch(function(err){console.log('Mjs-Gd: '+err); if(typeof onClose === 'function') onClose(); ig.gd.unfreez();});
            }else{
                onClose();
            };
        },
        isAvaiableRewarded: function(callback){
            if (gdsdk !== 'undefined' && gdsdk.preloadAd !== 'undefined'  && ig.gd.ready) {
                gdsdk
                .preloadAd('rewarded')
                .then(function(response){
                    console.log('Mjs-Gd: "Ad reward is avaiable"');
                    if(typeof callback === 'function') 
                        callback(true);
                }.bind(this))
                .catch(function(error){
                    console.log('Mjs-Gd: "Ad reward not available"')
                    if(typeof callback === 'function') 
                        callback(false);
                }.bind(this));
            }else
                if(typeof callback === 'function') 
                    callback(false);
        },
    });

    ig.gd = new ig.Gd();
});