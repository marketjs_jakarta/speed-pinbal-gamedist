ig.module('game.entities.buttons.btn-gameover')
.requires(
	'impact.entity',
     'game.entities.buttons.btn'
)
.defines(function() {
     // HOME BUTTON
	EntityBtnGameover = EntityBtn.extend({
          logoImg: new ig.Image('media/graphics/game/ui/btn_home.png', 106, 116),
          size:{x:106, y:116},
          name: 'btnHome',
          zIndex: 2010,
          isEnabled: false,
		isPause: false,
          posOffset: {x:0, y:0},

		update:function(){
			this.parent();
			if(this.isPause){
				this.pos.x = this.settings.pos.x + this.posOffset.x;
				this.pos.y = this.settings.pos.y + this.posOffset.y;
			}
		},

	    interact:function(){
		   ig.gd.show(this.homeGame.bind(this));
	    },
		
		homeGame:function(){
			ig.game.director.jumpTo(LevelMainmenu);
		},
     });

     // RESTART BUTTON
	EntityBtnRestart = EntityBtn.extend({
          logoImg: new ig.Image('media/graphics/game/ui/btn_restart.png', 106, 116),
          size:{x:106, y:116},
          name: 'btnRestart',
          zIndex: 2010,
          isEnabled: false,
		isPause: false,
          posOffset: {x:0, y:0},

		update:function(){
			this.parent();
			if(this.isPause){
				this.pos.x = this.settings.pos.x + this.posOffset.x;
				this.pos.y = this.settings.pos.y + this.posOffset.y;
			}
		},

        interact:function(){
			ig.gd.show(this.restarting.bind(this));
        },
		
		restarting:function(){
			ig.game.director.jumpTo(LevelTestGame);
		},
     });
});
