ig.module('game.entities.buttons.btn')
.requires(
	'impact.entity'
)
.defines(function() {
	EntityBtn = ig.Entity.extend({
		type:ig.Entity.TYPE.A,
		gravityFactor:0,

		logo: null,
		zIndex:20,

		isClicking: false,
		isEnabled: true,
		isLocked: 0,
		init:function(x,y,settings){
			ig.game.sortEntitiesDeferred();
			this.parent(x,y,settings);
		},

		update:function(){
			this.parent();

			if(this.isLocked > 0)		this.isLocked--;
		},

        	draw:function() {
			// Check if this button is being clicked
			if( this.isClicking ){
				if( !this.checkMousePos() ){
					this.isClicking = false;
				}
			}

			// Draw the button
			var drawX = (this.isClicking) ? this.size.x : 0;
			this.logoImg.draw(this.pos.x, this.pos.y, drawX, 0, this.size.x, this.size.y);
	  	},

		checkMousePos:function(){
			var pointer = ig.game.getEntitiesByType(EntityPointer)[0];
			pointer.refreshPos();

			return 	(	pointer.pos.x >= this.pos.x &&
						pointer.pos.x <= this.pos.x + this.size.x &&
						pointer.pos.y >= this.pos.y &&
						pointer.pos.y <= this.pos.y + this.size.y );
		},

		/*
			BUTTON CONTROLS
		*/

        	clicked:function() {//console.log('clicked ' + this.isClicking);
			if(!this.isClicking && this.isEnabled && this.isLocked <= 0){
				this.isClicking = true;
			}
		},

		clicking:function() {//console.log('clicking ' + this.isClicking);
			if(!this.isClicking && this.isEnabled && this.isLocked <= 0){
				this.isClicking = true;
			}
		},

		released:function() {//console.log('released ' + this.isClicking);
			if(this.isClicking && this.isEnabled){
				this.isClicking = false;
				ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.click);
				if(!this.isEnabled){
					return;
				}
				this.interact();
				this.isLocked = 3;
			}
		},

		interact:function(){

		}
	});
});
