ig.module('game.entities.buttons.btn-game')
.requires(
	'impact.entity',
     'game.entities.buttons.btn'
)
.defines(function() {
     // PAUSE BUTTON
	EntityBtnGame = EntityBtn.extend({
          logoImg: new ig.Image('media/graphics/game/ui/btn_pause.png', 51, 56),
          size:{x:51, y:56},
          name: 'btnPause',
          zIndex: 50,

          interact:function(){
			var gameControl = ig.game.getEntitiesByType(EntityGameControl)[0];
               gameControl._gamePause(true);
			this.isEnabled = false;
          }
     });
});
