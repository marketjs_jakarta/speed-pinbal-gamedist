ig.module('game.entities.buttons.btn-mainmenu')
.requires(
	'impact.entity',
     'game.entities.buttons.btn'
)
.defines(function() {
     // PLAY BUTTON
	EntityBtnMainmenu = EntityBtn.extend({
          logoImg: new ig.Image('media/graphics/game/menu/btn_play.png', 280, 151),
          size:{x:140, y:151},
          name: 'btnPlay',

          interact:function(){
			  this.playGame();
			//ig.gd.show(this.playGame.bind(this));
          },
		  
		  playGame:function(){
			var mainMenuControl = ig.game.getEntitiesByType(EntityMenuControl)[0];
			if(!mainMenuControl.hasPanel) mainMenuControl._promptTutorial();
		  },
     });

     // UPGRADES BUTTON
	EntityBtnUpgrades = EntityBtn.extend({
		logoImg: new ig.Image('media/graphics/game/menu/btn_upgrade.png', 212, 116),
		size:{x:106, y:116},
		name: 'btnPlay',

		interact:function(){
			ig.gd.show(this.upgrading.bind(this));
		},
		
		upgrading:function(){
			var upgWindow = ig.game.getEntitiesByType(EntityUpgrades)[0],
				mainMenuControl = ig.game.getEntitiesByType(EntityMenuControl)[0];
			if(upgWindow == null && !mainMenuControl.hasPanel){
				ig.game.spawnEntity(EntityUpgrades, 550, 0);
				mainMenuControl.hasPanel = true;
			}

			// Disable main menu buttons
			var 	btnPlay = ig.game.getEntitiesByType(EntityBtnMainmenu)[0],
				btnSettings = ig.game.getEntitiesByType(EntityBtnSettings)[0];
			if(btnPlay)			btnPlay.isEnabled = false;
			if(btnSettings)		btnSettings.isEnabled = false;
			this.isEnabled = false;

			if(_SETTINGS.MoreGames.Enabled){
				var moregames = ig.game.getEntitiesByType(EntityButtonMoreGames)[0];
				moregames.hide();
			}
		}
     });

     // SETTINGS BUTTON
	EntityBtnSettings = EntityBtn.extend({
          logoImg: new ig.Image('media/graphics/game/menu/btn_settings.png', 212, 116),
          size:{x:106, y:116},
          name: 'btnPlay',

          interact:function(){
			var settings = ig.game.getEntitiesByType(EntitySettings)[0],
				mainMenuControl = ig.game.getEntitiesByType(EntityMenuControl)[0];
			if(settings == null && !mainMenuControl.hasPanel){
				ig.game.spawnEntity(EntitySettings, 550, 0);
				mainMenuControl.hasPanel = true;
			}

			// Disable main menu buttons
			var 	btnPlay = ig.game.getEntitiesByType(EntityBtnMainmenu)[0],
				btnUpgrades = ig.game.getEntitiesByType(EntityBtnUpgrades)[0];
			if(btnPlay)			btnPlay.isEnabled = false;
			if(btnUpgrades)		btnUpgrades.isEnabled = false;
			this.isEnabled = false;

			if(_SETTINGS.MoreGames.Enabled){
				var moregames = ig.game.getEntitiesByType(EntityButtonMoreGames)[0];
				moregames.hide();
			}
          }
     });

	// TUTORIAL YES
	EntityBtnTutorialYes = EntityBtn.extend({
          logoImg: new ig.Image('media/graphics/game/ui/btn_yes.png', 116, 116),
          size:{x:106, y:116},
          name: 'btnYes',
		zIndex: 2001,
		tutorial: null, posOffset: {x:340, y:0},

		update:function(){
			this.parent();
			this.pos.x = this.tutorial.pos.x + this.posOffset.x;
			this.pos.y = this.tutorial.pos.y + this.posOffset.y;
		},

		interact:function(){
			ig.game.tutorial = true;
			ig.game.director.jumpTo(LevelTestGame);
		}
     });

	// TUTORIAL NO
	EntityBtnTutorialNo = EntityBtn.extend({
          logoImg: new ig.Image('media/graphics/game/ui/btn_no.png', 116, 116),
          size:{x:106, y:116},
          name: 'btnNo',
		zIndex: 2001,
		tutorial: null, posOffset: {x:100, y:0},

		update:function(){
			this.parent();
			this.pos.x = this.tutorial.pos.x + this.posOffset.x;
			this.pos.y = this.tutorial.pos.y + this.posOffset.y;
		},

          interact:function(){
			ig.game.tutorial = false;
			ig.game.director.jumpTo(LevelTestGame);
          }
     });
});
