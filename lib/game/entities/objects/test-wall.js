ig.module('game.entities.objects.test-wall')
.requires(
	'plugins.box2d.entity'
)
.defines(function() {
	EntityTestWall = ig.Box2DEntity.extend({

		name: 'obstacleArc',

		// Sprite properties
		animSheet: new ig.AnimationSheet('media/graphics/game/arc.png', 370, 195),
		size: {x:370, y:195},

		// Physics - Box2D
		box2dType: 2,
		dynamicType: 2, // entity collision type 0 dynamice , 1 kinematic , 2 static
		density: 0.1,
		friction: 1,
		restitution: 0, //entiy restitution
		vertices: [{x: -18.42, y: 6.80},{x: -16.37, y: 6.79},{x: -16.46, y: 9.67},{x: -18.50, y: 9.75}],

		init:function(x,y,settings){
			//this.vertices = this.circlevertices(3, 0.5);
			//this.vertices = this.createVertices();
			this.parent(x,y,settings);
			this.addAnim('idle', 1, [0]);

			if (!ig.global.wm){
				this.createParts();
			}
		},

		update:function(){this.parent();},

		beginContact: function (other) { //console.log('contact!' + other.name);
			if((other.name === 'gem' || other.name === 'gem2' || other.name === 'magnet') && other.body && !other.mag_isPulled){
				//if(other.posLock <= 14){
					//other.isReady = false;
					//other.posLock = 0;
					// console.log('colliding with obstacle, repositioning');
					// var newPos = other._reposition();
					// other.isReady = true;
                         //
					// other.posX = newPos.x; other.posY = newPos.y;
				//}
			}
		},

		preSolve: function(other){

		},

		postSolve: function(other, contact){

		},

		/*
			VERTICES
		*/
		parts: [],
		createParts: function(){
			 ig.game.spawnEntity(EntityTestWall2, 	this.pos.x + this.size.x / 2 - 8,
													 this.pos.y + this.size.y / 2 - 9
													 );

			ig.game.spawnEntity(EntityTestWall3, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9
													);

			ig.game.spawnEntity(EntityTestWall4, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9
													);

			ig.game.spawnEntity(EntityTestWall5, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9
													);

			ig.game.spawnEntity(EntityTestWall6, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9
													);

			ig.game.spawnEntity(EntityTestWall7, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9
													);

			ig.game.spawnEntity(EntityTestWall8, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9
													);

			ig.game.spawnEntity(EntityTestWall9, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9
													);

			ig.game.spawnEntity(EntityTestWall10, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9
													);

			ig.game.spawnEntity(EntityTestWall11, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9
													);

			ig.game.spawnEntity(EntityTestWall12, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9
													);

			ig.game.spawnEntity(EntityTestWall13, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9
													);

			ig.game.spawnEntity(EntityTestWall14, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9
													);

			ig.game.spawnEntity(EntityTestWall15, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9
													);

			ig.game.spawnEntity(EntityTestWall16, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9
													);

			ig.game.spawnEntity(EntityTestWall17, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9
													);

			ig.game.spawnEntity(EntityTestWall18, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9
													);

			ig.game.spawnEntity(EntityTestWall19, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9
													);
			/*var jointDef = new Box2D.Dynamics.Joints.b2RevoluteJointDef();

			jointDef.bodyA = newPart.body;
			jointDef.bodyB = this.body;
			jointDef.collideConnected = false;
			jointDef.localAnchorA = new Box2D.Common.Math.b2Vec2(0, 0);
			jointDef.localAnchorB = new Box2D.Common.Math.b2Vec2(0, 0);

			ig.world.CreateJoint(jointDef);
			this.parts.push(newPart);*/
		}
	});

	EntityTestWallParts = ig.Box2DEntity.extend({
		name: 'obstacleArc',

		// Physics - Box2D
		box2dType: 2,
		dynamicType: 2, // entity collision type 0 dynamice , 1 kinematic , 2 static
		density: 0.1,
		friction: 1,
		restitution: 0, //entiy restitution

		init:function(x,y,settings){
			this.parent(x,y,settings);
		},

		beginContact: function (other) { //console.log('contact!' + other.name);
			if(other.name === 'gem'){
				//other._reposition();
			}
		},

		update:function(){this.parent();}
	});

	EntityTestWall2 = EntityTestWallParts.extend({
		vertices: [{x: -18.04, y: 4.18}, {x: -15.87, y: 4.26}, {x: -16.37, y: 6.79}, {x: -18.42, y: 6.80}]
	});

	EntityTestWall3 = EntityTestWallParts.extend({
		vertices: [{x: -17.08, y: 1.43},{x: -14.97, y: 1.60},{x: -15.87, y: 4.26},{x: -18.04, y: 4.18}]
	});

	EntityTestWall4 = EntityTestWallParts.extend({
		vertices: [{x: -15.65, y: -1.32}, {x: -13.61, y: -0.70}, {x: -14.97, y: 1.60}, {x: -17.08, y: 1.43}]
	});

	EntityTestWall5 = EntityTestWallParts.extend({
		vertices: [{x: -13.67, y: -3.78},{x: -11.76, y: -2.82},{x: -13.61, y: -0.70},{x: -15.65, y: -1.32}]
	});

	EntityTestWall6 = EntityTestWallParts.extend({
		vertices: [{x: -11.22, y: -6.07}, {x: -9.82, y: -4.52}, {x: -11.76, y: -2.82}, {x: -13.67, y: -3.78}]
	});

	EntityTestWall7 = EntityTestWallParts.extend({
		vertices: [{x: -8.20, y: -7.83},{x: -7.44, y: -6.00},{x: -9.82, y: -4.52},{x: -11.22, y: -6.07}]
	});

	EntityTestWall8 = EntityTestWallParts.extend({
		vertices: [{x: -6.29, y: -8.59}, {x: -5.62, y: -6.76}, {x: -7.44, y: -5.95}, {x: -7.94, y: -8.00}]
	});

	EntityTestWall9 = EntityTestWallParts.extend({
		vertices: [{x: -4.56, y: -9.12},{x: -4.31, y: -7.10},{x: -5.62, y: -6.76},{x: -6.29, y: -8.59}]
	});

	EntityTestWall10 = EntityTestWallParts.extend({
		vertices: [{x: -2.47, y: -9.53}, {x: -2.47, y: -7.52}, {x: -4.31, y: -7.10}, {x: -4.56, y: -9.12}]
	});

	EntityTestWall11 = EntityTestWallParts.extend({
		vertices: [{x: 0.09, y: -9.73},{x: -0.19, y: -7.55},{x: -2.47, y: -7.52},{x: -2.47, y: -9.53}]
	});

	EntityTestWall12 = EntityTestWallParts.extend({
		vertices: [{x: 0.09, y: -9.73}, {x: 3.94, y: -9.28}, {x: 3.13, y: -7.29}, {x: -0.19, y: -7.55}]
	});

	EntityTestWall13 = EntityTestWallParts.extend({
		vertices: [{x: 7.67, y: -8.05},{x: 6.76, y: -6.15},{x: 3.13, y: -7.29},{x: 3.94, y: -9.28}]
	});

	EntityTestWall14 = EntityTestWallParts.extend({
		vertices: [{x: 7.67, y: -8.05}, {x: 10.88, y: -6.15}, {x: 9.18, y: -4.95}, {x: 6.76, y: -6.15}]
	});

	EntityTestWall15 = EntityTestWallParts.extend({
		vertices: [{x: 13.25, y: -4.26},{x: 11.51, y: -3.05},{x: 9.18, y: -4.95},{x: 10.88, y: -6.15}]
	});

	EntityTestWall16 = EntityTestWallParts.extend({
		vertices: [{x: 13.25, y: -4.26}, {x: 15.26, y: -1.80}, {x: 13.58, y: -0.86}, {x: 11.51, y: -3.05}]
	});

	EntityTestWall17 = EntityTestWallParts.extend({
		vertices: [{x: 17.16, y: 1.89},{x: 15.10, y: 2.06},{x: 13.58, y: -0.86},{x: 15.26, y: -1.80}]
	});

	EntityTestWall18 = EntityTestWallParts.extend({
		vertices: [{x: 17.26, y: 1.87}, {x: 18.21, y: 5.60}, {x: 16.28, y: 5.60}, {x: 15.10, y: 2.06}]
	});

	EntityTestWall19 = EntityTestWallParts.extend({
		vertices: [{x: 18.51, y: 9.70},{x: 16.40, y: 9.62},{x: 16.28, y: 5.60},{x: 18.21, y: 5.60}]
	});

	//
});
