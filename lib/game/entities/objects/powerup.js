ig.module('game.entities.objects.powerup')
.requires(
	'plugins.box2d.entity'
)
.defines(function() {

	// MAIN ENTITY
	EntityPowerup = ig.Box2DEntity.extend({
		// Physics - Box2D
		box2dType: 2,
		dynamicType: 0, // entity collision type 0 dynamice , 1 kinematic , 2 static
		density: 0.000001,
		friction: 0,
		restitution: 0, //entiy restitution

		// Sprite animation
		curScale: 0,
		curInc: 0.1,

		tutorial: false,
		isTaken: false,
		vertices: [{x: -2.81, y: -2.61},{x: 2.92, y: -2.66},{x: 3.02, y: 3.07},{x: -2.81, y: 2.92}],

		init: function(x,y,settings){
			this.parent(x,y,settings);
		},

		draw: function(){
			var ctx = ig.system.context;
			ctx.save();

			if(this.posLock >= 5)
			ctx.drawImage(	this.imgSheet.data, 0, 0,
							this.imgSheet.width,
							this.imgSheet.height,
							this.pos.x + (this.imgSheet.width / 2) * (1-this.curScale),
							this.pos.y + (this.imgSheet.height / 2) * (1-this.curScale),
							this.imgSheet.width * this.curScale,
							this.imgSheet.height * this.curScale  );
			ctx.restore();
		},

		isReady: false,
		posX: 0, posY: 0,
		posLock: 0,
		update: function(){
			// Reposition after spawn
			if(!this.isReady){
				var newPos = this._reposition();
				this.isReady = true;

				this.posX = newPos.x; this.posY = newPos.y;
			}
			else{
				this.body.SetPositionAndAngle(new Box2D.Common.Math.b2Vec2(this.posX, this.posY), 0);
				if(this.posLock <= 5 && !ig.game.box2dPaused)	this.posLock++;
			}

			// Sprite scale
			if(this.curScale < 1){
				this.curScale += this.curInc;

				if(this.curScale > 1)	this.curScale = 1;
			}

			this.parent();
		},

		/*
		   _____      _ _ _     _
		  / ____|    | | (_)   (_)
		 | |     ___ | | |_ ___ _  ___  _ __
		 | |    / _ \| | | / __| |/ _ \| '_ \
		 | |___| (_) | | | \__ \ | (_) | | | |
		  \_____\___/|_|_|_|___/_|\___/|_| |_|

			COLLISION TO WALLS / OBSTACLES
		*/
		beginContact: function (other, contact) {
			// if(other.name != 'ball' && this.posLock <= 5){
			// 	if(this.body){
			// 		this.body.SetPosition(new Box2D.Common.Math.b2Vec2(-500, -500));
			// 		this.isReady = false;
			// 	}
			// }
		},

		animNull: false, repositioning: false,
		_reposition: function(){
			// Respawn if inside obstacle
			var minX = 7, maxX = 45, minY = 20, maxY = 80;
			if(this.tutorial){
				var	randX = 320 * Box2D.SCALE,
					randY = 300 * Box2D.SCALE;
			}else{
				var	randX = Math.floor( Math.random() * (maxX - minX)) + minX,
					randY = Math.floor( Math.random() * (maxY - minY)) + minY;
			}

			if(this.body)
			this.body.SetPositionAndAngle(new Box2D.Common.Math.b2Vec2(randX, randY), 0);
			this.currentAnim = null;

			this.repositioning = true;
			return {x:randX, y:randY};
		},

		preSolve: function(other, contact){
			if(other.name === 'ball')
				contact.SetEnabled(false);

			if(other.name)
				if((other.name.indexOf('obstacle')!==-1 || other.name == 'bouncer')){
					if(this.mag_isPulled){
						contact.SetEnabled(false);
					}
				}

			if(other.name && this.body)
				if((other.name.indexOf('obstacle')!==-1 || other.name == 'bouncer') && this.posLock <= 7){
					var newPos = this._reposition();
					this.isReady = true;

					this.posX = newPos.x; this.posY = newPos.y;
					console.log('repositioning this gem because of collision with ' + other.name + ', ' + this.posLock);
					this.posLock++;
					if(this.posLock >= 7){
						var randPos = Math.random();
						if(randPos <= 0.1)				{this.posX = 246; this.posY = 499;}
						else if(randPos <= 0.2)			{this.posX = 370; this.posY = 389;}
						else if(randPos <= 0.3)			{this.posX = 330; this.posY = 225;}
						else if(randPos <= 0.4)			{this.posX = 176; this.posY = 208;}
						else if(randPos <= 0.5)			{this.posX = 138; this.posY = 393;}
						else if(randPos <= 0.6)			{this.posX = 194; this.posY = 589;}
						else if(randPos <= 0.7)			{this.posX = 303; this.posY = 599;}
						else if(randPos <= 0.8)			{this.posX = 194; this.posY = 688;}
						else if(randPos <= 0.9)			{this.posX = 319; this.posY = 695;}
						else							{this.posX = 258; this.posY = 652;}
					}
				}
		},

		postSolve: function(other, contact){

		}
	});

	/*
	  ______       _   _ _   _
	 |  ____|     | | (_) | (_)
	 | |__   _ __ | |_ _| |_ _  ___  ___
	 |  __| | '_ \| __| | __| |/ _ \/ __|
	 | |____| | | | |_| | |_| |  __/\__ \
	 |______|_| |_|\__|_|\__|_|\___||___/

		ENTITY VARIANTS
	*/
	EntityMagnet = EntityPowerup.extend({
		name: 'magnet',
		imgSheet: new ig.Image('media/graphics/game/magnet.png', 31, 45),
		size:{x:31, y:45},
		//vertices: [{x: -1.55, y: -2.25},{x: 1.55, y: -2.25},{x: 1.54, y: 1.21},{x: 0.55, y: 2.28},{x: -0.50, y: 2.33},{x: -1.51, y: 1.15}]
	});
});
