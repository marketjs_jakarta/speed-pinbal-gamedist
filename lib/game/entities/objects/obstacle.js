ig.module('game.entities.objects.obstacle')
.requires(
	'plugins.box2d.entity'
)
.defines(function() {
	EntityObstacle = ig.Box2DEntity.extend({
		name: 'obstacle',

		// Physics - Box2D
		box2dType: 2,
		dynamicType: 2, // entity collision type 0 dynamice , 1 kinematic , 2 static
		density: 0.1,
		friction: 0,
		restitution: 0, //entiy restitution
		curScale:0, curInc: 0.1,

		init:function(x,y,settings){
			this.parent(x,y,settings);
			if (ig.global.wm) return;

			var 	ents = ig.game.entities,
				len = ents.length;
			for(var i = 0; i < len; i++){
				if(ents[i].name === 'gem' || ents[i].name === 'gem2' || ents[i].name === 'magnet'){
					//ents[i].isReady = false;
					//ents[i].body.SetPosition(new Box2D.Common.Math.b2Vec2(-500, -500));
					//ents[i].isReady = false;
					ents[i].posLock = 0;
					//ents[i]._reposition();
				}
			}
		},

		beginContact: function (other) {
			if((other.name === 'gem' || other.name === 'gem2' || other.name === 'magnet') && other.body && !other.mag_isPulled){
				if(other.posLock <= 14){
					//other.isReady = false;
					//other.posLock = 0;
					// console.log('colliding with obstacle, repositioning');
					// var newPos = other._reposition();
					// other.isReady = true;
                         //
					// other.posX = newPos.x; other.posY = newPos.y;
				}
			}
		},

		preSolve: function(other, contact){

		},

		postSolve: function(other, contact){

		},

		draw: function(){
			var ctx = ig.system.context;
			ctx.save();

			ctx.drawImage(	this.imgSheet.data, 0, 0,
							this.imgSheet.width,
							this.imgSheet.height,
							this.pos.x + (this.imgSheet.width / 2) * (1-this.curScale),
							this.pos.y + (this.imgSheet.height / 2) * (1-this.curScale),
							this.imgSheet.width * this.curScale,
							this.imgSheet.height * this.curScale  );

			ctx.restore();
		},

		update: function(){
			this.parent();

			// Sprite scale
			if(this.curScale < 1){
				this.curScale += this.curInc;

				if(this.curScale > 1)	this.curScale = 1;
			}
		},
	});

	EntityObstacle1 = EntityObstacle.extend({
		name: 'obstacle',

		imgSheet: new ig.Image('media/graphics/game/obs/obs1.png', 40, 46),
		size: {x:40, y:46},
		offset:{x:0, y:-4},
		//vertices:
		box2dType: 1,
	});

	EntityObstacle2 = EntityObstacle.extend({
		restitution: 0.4,
		name: 'bouncer',

		imgSheet: new ig.Image('media/graphics/game/obs/obs2.png', 40, 46),
		size: {x:40, y:46},
		offset:{x:0, y:-4},
		//vertices:
		box2dType: 1,
	});

	EntityObstacle3 = EntityObstacle.extend({
		name: 'obstacle',

		imgSheet: new ig.Image('media/graphics/game/obs/obs3.png', 86, 61),
		size: {x:86, y:61},
		vertices: [{x: -0.01, y: -3.02},{x: 4.22, y: 2.58},{x: -4.12, y: 2.48}],
		box2dType: 2,
	});

	EntityObstacle4 = EntityObstacle.extend({
		restitution: 0.5,
		name: 'bouncer',

		imgSheet: new ig.Image('media/graphics/game/obs/obs4.png', 72, 106),
		size: {x:72, y:106},
		vertices: [{x: 3.60, y: -5.30},{x: 3.56, y: 1.33},{x: -3.56, y: 4.65}],
		box2dType: 2,
	});

	EntityObstacle5 = EntityObstacle.extend({
		name: 'obstacle',

		imgSheet: new ig.Image('media/graphics/game/obs/obs5.png', 94, 150),
		size: {x:94, y:150},
		vertices: [{x: -4.52, y: -0.47},{x: 4.50, y: -7.27},{x: 4.72, y: 6.69}],
		box2dType: 2,
	});

	EntityObstacle6 = EntityObstacle.extend({
		name: 'obstacle',

		imgSheet: new ig.Image('media/graphics/game/obs/obs6.png', 52, 70),
		size: {x:52, y:70},
		vertices: [{x: -2.62, y: 1.03},{x: -2.58, y: -1.92},{x: -0.01, y: -3.48},{x: 2.52, y: -1.94},{x: 2.48, y: 1.04},{x: -0.04, y: 2.53}],
		box2dType: 2,
	});

	EntityObstacle7 = EntityObstacle.extend({
		name: 'obstacle',

		imgSheet: new ig.Image('media/graphics/game/obs/obs7.png', 51, 58),
		size: {x:51, y:58},
		vertices: [{x: -2.55, y: -2.90},{x: 2.52, y: -2.90},{x: 2.52, y: 2.10},{x: -2.57, y: 2.12}],
		box2dType: 2,
	});

	EntityObstacle8 = EntityObstacle.extend({
		name: 'obstacle',

		imgSheet: new ig.Image('media/graphics/game/obs/obs8.png', 137, 46),
		size: {x:137, y:46},
		vertices: [{x: -6.85, y: -2.30},{x: 6.85, y: -2.30},{x: 6.80, y: 1.60},{x: -6.80, y: 1.59}],
		box2dType: 2,
	});

	EntityObstacle9 = EntityObstacle.extend({
		restitution: 1,
		name: 'bouncer',

		imgSheet: new ig.Image('media/graphics/game/obs/obs9.png', 72, 106),
		size: {x:72, y:106},
		vertices: [{x: -3.60, y: 1.25},{x: -3.58, y: -5.32},{x: 3.51, y: 4.72}],
		box2dType: 2,
	});

	EntityObstacle10 = EntityObstacle.extend({
		name: 'obstacle',

		imgSheet: new ig.Image('media/graphics/game/obs/obs10.png', 94, 150),
		size: {x:94, y:150},
		vertices: [{x: -4.68, y: -7.48},{x: 4.65, y: -0.53},{x: -4.70, y: 6.50}],
		box2dType: 2,
	});
});
