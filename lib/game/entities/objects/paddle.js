ig.module('game.entities.objects.paddle')
.requires(
	'plugins.box2d.entity'
)
.defines(function() {
	EntityPaddle = ig.Box2DEntity.extend({
		name: 'paddle',

		// Sprite properties
		animSheet: new ig.AnimationSheet('media/graphics/game/paddle.png', 99, 44),
		size: {x:99, y:44},

		// Physics - Box2D
		box2dType: 2,
		dynamicType: 0, // entity collision type 0 dynamice , 1 kinematic , 2 static
		density: 2,					// 0.5
		friction: 1,
		restitution: 0, //entiy restitution
		vertices: [{x: -4.98, y: -1.85},{x: 5.9, y: -0.59},{x: 5.9, y: 0.55},{x: -5.17, y: 2.05},{x: -5.07, y: -0.14}],

		// Settings
		motorSpeed: 25,				// 25, 23
		lowerAngle: -25,				// -20
		upperAngle: 17,				// 15, Flat is 0

		// Misc
		gameControl: null,
		isFlicking: false,
		touchInput: false,
		input1: 'QQQ', input2: 'Left',
		tutPos: {x:180.18914267889105, y:872.7711511416821, angle:0.2967059728390361},

		init:function(x,y,settings){
			this.parent(x,y,settings);


		},

		ready:function(){
			this.createJoint(-3);	// -3
			this.body.SetPositionAndAngle(new Box2D.Common.Math.b2Vec2(	this.tutPos.x * Box2D.SCALE,
															this.tutPos.y * Box2D.SCALE),
															this.tutPos.angle);
			this.addAnim('idle', 1, [0]);
		},

		update: function() {
			this.parent();
			if(ig.game.box2dPaused)	return;

			if(ig.input.state(this.input1) || ig.input.state(this.input2) || this.touchInput){
				this.joint.SetMotorSpeed(-this.motorSpeed);
				this.isFlicking = true;

				if(!this.soundPlay){
					ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.flipper);
					this.soundPlay = true;
				}
			}else{
				this.joint.SetMotorSpeed(this.motorSpeed);
				this.isFlicking = false;
				this.soundPlay = false;
			}
		},

		/*
			   _       _       _
			  | |     (_)     | |
			  | | ___  _ _ __ | |_ ___
		  _   | |/ _ \| | '_ \| __/ __|
		 | |__| | (_) | | | | | |_\__ \
		  \____/ \___/|_|_| |_|\__|___/

			JOINTS
		*/

		box:null,
		joint: null,

		createJoint: function (dx) {
			var jointDef = new Box2D.Dynamics.Joints.b2RevoluteJointDef();
			this.box = ig.game.spawnEntity(EntityPaddleJoint,
											this.pos.x, this.pos.y,
											{hostEntity: this});
			jointDef.bodyA = this.box.body;
			jointDef.bodyB = this.body;
			jointDef.collideConnected = false;
			jointDef.localAnchorA = new Box2D.Common.Math.b2Vec2(0, 0);
			jointDef.localAnchorB = new Box2D.Common.Math.b2Vec2(dx, 0);

			jointDef.enableLimit = true;
			jointDef.lowerAngle = this.lowerAngle * Math.PI/180;
			jointDef.upperAngle = this.upperAngle * Math.PI/180;

			jointDef.enableMotor = true;
			jointDef.maxMotorTorque = 500000;						// 450,000
			jointDef.motorSpeed = 0;

			this.joint = ig.world.CreateJoint(jointDef);
		},

		/*
	   		_____      _ _ _     _
		    / ____|    | | (_)   (_)
		   | |     ___ | | |_ ___ _  ___  _ __
		   | |    / _ \| | | / __| |/ _ \| '_ \
		   | |___| (_) | | | \__ \ | (_) | | | |
		    \_____\___/|_|_|_|___/_|\___/|_| |_|

		   COLLISION
		*/
		endContact: function (other, contact) {
			if(other.name != 'ball')
				return;

			if(other.body.m_linearVelocity.y < -50){
				// SURE SHOT
				if(this.isFlicking && this.gameControl.ss_available){
					other.ss_willFire = true;

					this.gameControl.ss_interval = 0;
					this.gameControl.ss_available = false;
				}
			}
		},

		preSolve: function (other, contact, argument) {
			if(other.name)
				if(other.name.indexOf('obstacle')!==-1)
					contact.SetEnabled(false);
		}
	});

	/*
	    	 _____  _       _     _     _____          _     _ _
		|  __ \(_)     | |   | |   |  __ \        | |   | | |
		| |__) |_  __ _| |__ | |_  | |__) |_ _  __| | __| | | ___
		|  _  /| |/ _` | '_ \| __| |  ___/ _` |/ _` |/ _` | |/ _ \
		| | \ \| | (_| | | | | |_  | |  | (_| | (_| | (_| | |  __/
		|_|  \_\_|\__, |_| |_|\__| |_|   \__,_|\__,_|\__,_|_|\___|
				__/ |
			    |___/
	     RIGHT PADDLE
	*/
	EntityPaddleRight = EntityPaddle.extend({
		animSheet: new ig.AnimationSheet('media/graphics/game/paddlerot.png', 99, 44),

		// Settings
		motorSpeed: -25,				// -25, -23
		lowerAngle: 160,				// 165, Flat is 180
		upperAngle: 205,				// 200
		input1: 'EEE', input2: 'Right',
		tutPos: {x:366.68443232891053, y:872.7711511416821, angle:2.7576202181510405},

		update: function() {
			this.parent();
			if(ig.game.box2dPaused)	return;

			if(ig.input.state(this.input1) || ig.input.state(this.input2) || this.touchInput){
				this.joint.SetMotorSpeed(-this.motorSpeed);

				if(!this.soundPlay){
					ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.flipper);
					this.soundPlay = true;
				}
			}else{
				this.joint.SetMotorSpeed(this.motorSpeed);
				this.soundPlay = false;
			}
		},
	});

	/*
		_____           _                   _       _       _
	    / ____|         | |                 | |     (_)     | |
	   | |     ___ _ __ | |_ ___ _ __       | | ___  _ _ __ | |_
	   | |    / _ \ '_ \| __/ _ \ '__|  _   | |/ _ \| | '_ \| __|
	   | |___|  __/ | | | ||  __/ |    | |__| | (_) | | | | | |_
	    \_____\___|_| |_|\__\___|_|     \____/ \___/|_|_| |_|\__|

	   CENTER JOINT
	*/
	EntityPaddleJoint = ig.Box2DEntity.extend({
		box2dType: 1,
		dynamicType: 1, // entity collision type 0 dynamice , 1 kinematic , 2 static
		density: 0.5,
		friction: 0,
		restitution: 0, //entiy restitution
		gravityFactor: 0,

		init: function(x, y, settings){
			this.parent(x, y, settings);
		}
	});
});
