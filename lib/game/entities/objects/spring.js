ig.module('game.entities.objects.spring')
.requires(
	'plugins.box2d.entity'
)
.defines(function() {
	EntitySpring = ig.Box2DEntity.extend({

		name: 'spring',

		// Sprite properties
		animSheet: new ig.AnimationSheet('media/graphics/game/spring.png', 110, 59),
		size: {x:110, y:59},
		zIndex: 20,

		// Physics - Box2D
		box2dType: 2,
		dynamicType: 2, // entity collision type 0 dynamice , 1 kinematic , 2 static
		density: 0.2,
		friction: 1,
		restitution: 0, //entiy restitution
		vertices: [{x: -5.50, y: -2.95},{x: 5.50, y: -2.95},{x: 5.52, y: -0.21},{x: -5.52, y: -0.23}],

		// Misc
		gameControl: null,
		hits: 1,

		init:function(x,y,settings){
			this.parent(x,y,settings);

			this.hits = ig.game.upg_spring;
			this.posY = y;

			this.addAnim('idle', 1, [0]);
		},

		isReady: 0,
		update: function() {
			if(this.moved){
				this.moveDur++;
				if(this.moveDur >= 30){
					this.moveDur = 0;
					this.moved = false;
					this.posY -= 20;
				}
			}
			this.parent();
			this.pos.y = this.posY;
		},

		// Hitting objects

		moved: false, moveDur: 0, posY: 0,
		beginContact: function (other, contact) {
			if(other.name !== 'ball')
				return;

			var xForce = Math.random() * 0.1;
			if(Math.random() <= 0.5)
				xForce = -xForce;

			var ballForce = new Box2D.Common.Math.b2Vec2(xForce, -1.5);
			other.body.ApplyImpulse(ballForce, other.body.GetPosition());
			contact.SetEnabled(false);

			this.gameControl.uiControl._announce(_STRINGS["Announcer"]["spring hit"], '#00fff6', 1.5);
			ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.sureShot);

			// Reduce spring health
			this.hits--;
			if(this.hits <= 0){
				this.kill();
				this.gameControl.uiControl._announce(_STRINGS["Announcer"]["spring dead"], '#00fff6', 1.5);
			}
			this.posY += 20;
			this.moved = true;
		}

	});
});
