ig.module('game.entities.objects.ball')
.requires(
	'plugins.box2d.entity'
)
.defines(function() {
	EntityBall = ig.Box2DEntity.extend({
		name: 'ball',

		// Sprite properties
		imgSheet: new ig.Image('media/graphics/game/ball.png', 29, 29),
		imgSheet_mag: new ig.Image('media/graphics/game/ballmagnet.png', 29, 29),
		size: {x:29, y:29},
		zIndex: 5,

		// Physics - Box2D
		box2dType: 1,
		dynamicType: 0, // entity collision type 0 dynamice , 1 kinematic , 2 static
		density: 0.001,
		friction: 0,
		restitution: 0, //entiy restitution

		// Misc
		gameControl: null,

		init:function(x,y,settings){
			this.parent(x,y,settings);

			//this.addAnim('idle', 1, [0]);
			this.gameControl = ig.game.getEntityByName('gameControl');

			this.center = {x: this.pos.x, y: this.pos.y};
		},

		isReady: 0,
		update: function() {
			// Initial force on ball
			if(this.isReady < 3){
				this.isReady++;

				if(this.isReady >= 3){
					var negative = (Math.random() <= 0.5) ? 0.5 : -0.5,
						forceAmt = 1 * negative,
						force = new Box2D.Common.Math.b2Vec2(forceAmt,0);
					this.body.ApplyImpulse(force, this.body.GetPosition());
				}
			}

			//var gravity = this.body.m_mass * -ig.game.gravity * Box2D.SCALE;
			//this.body.ApplyForce( new Box2D.Common.Math.b2Vec2(0,0.01), this.body.GetPosition() );
			if(!ig.game.box2dPaused){
				var gravity = new Box2D.Common.Math.b2Vec2(0, 600 * 1 * this.body.GetMass() * Box2D.SCALE);
				this.body.ApplyForce(gravity, this.body.GetPosition());
			}

			// Hitting objects
			this.isHit = false;

			// Special systems
			/*Magnet*/		this.mag_update();
			/*Sure Shot*/		this.ss_update();
			/*Get object*/		this.getObject();

			// Boundary
			if(this.pos.y >= 1200){
				this.kill();
				if(this.gameControl.bonusBallExist){
					this.gameControl.bonusBallExist = false;
					return;
				}
				else
					this.gameControl._spawnBall();

				if(this.mag_duration > 0){
					this.gameControl.mag_duration = this.mag_duration;
				}else{
					this.gameControl.mag_duration = 0;
				}
			}

			// Max Velocity / Max Speed Limit
			var maxVelocity = 1600 * Box2D.SCALE;
			var currentVelocity = this.body.GetLinearVelocity();
			//console.log(currentVelocity.Length() + ', ' + maxVelocity);
		     if(currentVelocity.Length() > maxVelocity){
				var newVelocity = currentVelocity.Copy();
				newVelocity.Normalize();
				newVelocity.Multiply(maxVelocity);
				this.body.SetLinearVelocity(newVelocity);
			}

			if(this.obs_hit > 0){
				this.obs_hit--;
				if(this.obs_hit <= 0){
					this.obs_soundPlay = false;
				}
			}

			this.parent();
		},

		draw:function(){
			if(this.mag_duration > 0)		this.imgSheet_mag.draw(this.pos.x, this.pos.y);
			else 						this.imgSheet.draw(this.pos.x, this.pos.y);
		},

		/*
		   _____      _ _ _     _
		  / ____|    | | (_)   (_)
		 | |     ___ | | |_ ___ _  ___  _ __
		 | |    / _ \| | | / __| |/ _ \| '_ \
		 | |___| (_) | | | \__ \ | (_) | | | |
		  \_____\___/|_|_|_|___/_|\___/|_| |_|

			COLLISION SYSTEM
		*/
		isHit: false,
		force: null,
		xFacing: 1, yFacing: 1,

		obs_soundPlay: true,
		obs_hit: 30,
		lastContactPaddle: false,

		beginContact: function (other, contact) {
			if(other.name === 'bouncer'){
				ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.sureShot);
			}
			if(other.name)
				if(other.name.indexOf('obstacle')!==-1 || other.name === 'paddle'){
					if(!this.obs_soundPlay && this.obs_hit <= 0){
						var yVel = this.body.GetLinearVelocity().y;
						if(yVel > 70 || yVel < -70 || other.name === 'obstacleArc' || this.lastContactPaddle){
							ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.hitObstacle);
							this.obs_soundPlay = true;
							this.lastContactPaddle = false;
						}
					}
				}
			if(other.name === 'paddle')	this.lastContactPaddle = true;
			/*if(!this.getObject(other))   return;

			if(this.isHit){
				other.body.SetPosition(-500, -500);
				other.body = null;
				other.kill();
				contact.SetEnabled(false);

				force = new Box2D.Common.Math.b2Vec2(this.force.x,this.force.y);
				this.body.ApplyForce(force , this.body.GetPosition());

				return;
			}
			else{
				if(other.body){
					other.body.SetPosition(-500, -500);
					other.kill();

					var angle = this.body.GetAngle();
					if(angle <= 90){
						this.xFacing = 1; this.yFacing = -1;
					}else if(angle <= 180){
						this.xFacing = -1; this.yFacing = -1;
					}else if(angle <= 270){
						this.xFacing = -1; this.yFacing = 1;
					}else if(angle <= 360){
						this.xFacing = 1; this.yFacing = 1;
					}

					this.force = this.body.m_linearVelocity;
					this.force = new Box2D.Common.Math.b2Vec2(
						this.body.m_linearVelocity.x * this.xFacing,
						this.body.m_linearVelocity.y * this.yFacing
					);

					this.isHit = true;
				}
			}*/
		},

		endContact: function(other, contact){
			if(other.name)
				if(other.name.indexOf('obstacle')!==-1 || other.name === 'paddle'){
					if(this.obs_soundPlay){
						// if(other.name === 'obstacleArc')
						// 	this.obs_hit = 15;
						// else
						// 	this.obs_hit = 15;
						this.obs_hit = 15;
					}
				}
		},

		// Returns true if the colliding object is interactable
		//pickObject: function(other){
		getObject: function(){
			var 	ents = ig.game.entities,
				len = ents.length,
				other = null;
			for(var i = 0; i < len; i++){
				other = ents[i];
				if(other.name === 'gem' || other.name === 'gem2'){
					if(other.isTaken)	continue;
					var 	gemPosX 		= other.pos.x,
						gemPosY 		= other.pos.y,
						ballPosX 		= this.pos.x,
						ballPosY 		= this.pos.y;

					var distX 			= gemPosX - ballPosX;
					var distY 			= gemPosY - ballPosY;
					var distance 			= Math.sqrt(distX*distX + distY*distY);

					if(distance <= 50){
						/////// HIT GEM / GET GEM ///////
						ig.game.curGems++;
						if(!this.gameControl.bonusGem)	this.gameControl._spawnGem();
						else 						this.gameControl.bonusGem = false;
						ig.game.playerGems++;
						ig.game.io.storage.set('speed-pinball-gems', ig.game.playerGems);

						ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.getGem);

						// Spawn Obstacle Check
						this.gameControl.obsControl._spawnObs();

						var uiControl = ig.game.getEntityByName('uiControl');
						uiControl._createMovingText(
							_STRINGS["Game"]["GemGet"], 'Nord', 25, this.pos.x / 2, this.pos.y / 2, -0.33, 1, 1.5, '#FFFFFF'
						);

						// Announcer text
						if(!this.ss_isFiring){
							var timeLeft = (this.gameControl.cDown.delta() * -1) / this.gameControl.maxTime;
							timeLeft *= 100;

							if(timeLeft >= 80)			this.gameControl.uiControl._announce(_STRINGS["Announcer"]["ballHit3"], '#00fff6', 1.5);
							else if(timeLeft >= 45)		this.gameControl.uiControl._announce(_STRINGS["Announcer"]["ballHit2"], '#00fff6', 1.5);
							else						this.gameControl.uiControl._announce(_STRINGS["Announcer"]["ballHit1"], '#00fff6', 1.5);
						}else
							this.gameControl.uiControl._announce(_STRINGS["Announcer"]["sureShotHit"], '#fffc00', 1.5);

						// Check if spawning bonus gem
						if(ig.game.curGems % 5 == 0){
							this.gameControl.bonusGem = true;
							this.gameControl._spawnGem();
							this.gameControl.uiControl._announce(_STRINGS["Announcer"]["bonus gem"], '#04ca58', 2);
						}
						// Check if spawning bonus ball
						/*if(ig.game.curGems % 15 == 0 && !this.gameControl.bonusBallExist){
							this.gameControl.bonusBall = true;
							this.gameControl._spawnBall();
							this.gameControl.uiControl._announce(_STRINGS["Announcer"]["bonus ball"], '#04ca58', 2);
						}*/

						other.isTaken = true;
						//other.body.SetPosition(-500, -500);
						if(other.body) ig.world.DestroyBody(other.body);
						other.body = null;
						other.kill();
						//contact.SetEnabled(false);

						// this.force = this.body.m_linearVelocity;
						// force = new Box2D.Common.Math.b2Vec2(this.force.x,this.force.y);
						// this.body.ApplyForce(force , this.body.GetPosition());
						/////// HIT GEM / GET GEM ///////
					}
				}else if(other.name === 'magnet'){
					if(other.isTaken)	continue;
					var 	gemPosX 		= other.pos.x,
						gemPosY 		= other.pos.y,
						ballPosX 		= this.pos.x,
						ballPosY 		= this.pos.y;

					var distX 			= gemPosX - ballPosX;
					var distY 			= gemPosY - ballPosY;
					var distance 			= Math.sqrt(distX*distX + distY*distY);

					if(distance <= 40){
						this.mag_duration = 5 + 2 * ig.game.upg_magnet;
						this.gameControl.uiControl._announce(_STRINGS["Announcer"]["magnet"], '#04ca58', 2);
						ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.magnetOn);

						other.isTaken = true;
						//other.body.SetPosition(-500, -500);
						if(other.body) ig.world.DestroyBody(other.body);
						other.body = null;
						other.kill();
						//contact.SetEnabled(false);

						// this.force = this.body.m_linearVelocity;
						// force = new Box2D.Common.Math.b2Vec2(this.force.x,this.force.y);
						// this.body.ApplyForce(force , this.body.GetPosition());
					}
				}
			}

			/*if(other.name === 'gem' || other.name === 'gem2'){

			}
			else if(other.name === 'magnet'){

			}*/
		},

		preSolve: function(other, contact){
			////////////////////////////// TEST //////////////////////////////
			if(other.name === 'gem' || other.name === 'gem2' || other.name === 'magnet' /*|| other.name === 'obstacle' || other.name === 'bouncer'*/){
				contact.SetEnabled(false);
				return;
			}
			////////////////////////////// TEST //////////////////////////////

			if(other.name)
				if((other.name.indexOf('obstacle')!==-1 || other.name === 'bouncer') && this.ss_isFiring){
					contact.SetEnabled(false);
				}
		},

		/*
		  __  __                        _
		 |  \/  |                      | |
		 | \  / | __ _  __ _ _ __   ___| |_
		 | |\/| |/ _` |/ _` | '_ \ / _ \ __|
		 | |  | | (_| | (_| | | | |  __/ |_
		 |_|  |_|\__,_|\__, |_| |_|\___|\__|
						__/ |
					   |___/
			MAGNET SYSTEM
		*/
		mag_duration: 0,
		mag_distReq: 90,				// Distance required to pull gem

		mag_update: function(){
			if(this.mag_duration <= 0 || ig.game.box2dPaused)		return;

			// Reduce duration
			this.mag_duration -= ig.system.tick;
			if(this.mag_duration <= 0){
				this.gameControl.uiControl._announce(_STRINGS["Announcer"]["magnet over"], '#04ca58', 2);
				ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.magnetOff);
			}

			// Pull gems
			var ents = ig.game.entities;
			var len = ents.length;
			for(var i = 0; i < len; i++){
				if(ents[i].name === 'gem' || ents[i].name === 'gem2'){
					var gemPosX 		= ents[i].pos.x,
						gemPosY 		= ents[i].pos.y,
						ballPosX 		= this.pos.x,
						ballPosY 		= this.pos.y;

					var distX 			= gemPosX - ballPosX;
					var distY 			= gemPosY - ballPosY;
					var distance 			= Math.sqrt(distX*distX + distY*distY);

					if(distance <= this.mag_distReq){
						ents[i].mag_ball = this;
						ents[i].mag_isPulled = true;
					}
				}
			}
		},
		/*
		   _____                   _____ _           _
		  / ____|                 / ____| |         | |
		 | (___  _   _ _ __ ___  | (___ | |__   ___ | |_
		  \___ \| | | | '__/ _ \  \___ \| '_ \ / _ \| __|
		  ____) | |_| | | |  __/  ____) | | | | (_) | |_
		 |_____/ \__,_|_|  \___| |_____/|_| |_|\___/ \__|

		 SURE SHOT SYSTEM
		*/
		ss_willFire: false,
		ss_fireDelay: 0.01,
		ss_fireDelay_def: 0.01,
		ss_speed: 25,
		ss_angle: 0,

		ss_targetGem: null,
		ss_isFiring: false,
		ss_posX: 0, ss_posY: 0,
		ss_distReq: 25,

		ss_trailCount: 0,
		ss_trailCountMax: 2,

		ss_update: function(){
			if(!this.ss_willFire && !this.ss_isFiring)	return;

			// Delay before seeking the gem
			if(this.ss_fireDelay > 0){
				this.ss_fireDelay -= ig.system.tick;
			}
			// Search for target gem
			else if(!this.ss_isFiring){
				this.ss_targetGem = ig.game.getEntityByName('gem');
				if(this.ss_targetGem == null){
					this.ss_targetGem = ig.game.getEntityByName('gem2');
					if(this.ss_targetGem == null)
						return;
				}

				this.ss_isFiring = true;
				this.ss_posX = this.pos.x * Box2D.SCALE;
				this.ss_posY = this.pos.y * Box2D.SCALE;

				ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.sureShot);
				this.ss_trailCount = 0;
			}
			// Travel to target gem
			// If gem does not exist anymore, end sure-shot mode
			else if(this.ss_isFiring){
				if(!this.ss_targetGem){
					this.ss_targetGem = ig.game.getEntityByName('gem2');
					if(!this.ss_targetGem){
						this.ss_end();
						return;
					}
				}

				this.ss_angle = Math.atan2(	this.pos.y - this.ss_targetGem.pos.y,
											this.pos.x - this.ss_targetGem.pos.x ) * 180 / Math.PI;
				var forceX = this.ss_speed * Math.cos((this.ss_angle * Math.PI)/180),
					forceY = this.ss_speed * Math.sin((this.ss_angle * Math.PI)/180);

				// Ball Trail
				this.ss_trailCount++;
				if(this.ss_trailCount >= this.ss_trailCountMax)
					ig.game.spawnEntity(EntityTrail, this.pos.x, this.pos.y);

				// Move ball
				this.ss_posX -= forceX * Box2D.SCALE;
				this.ss_posY -= forceY * Box2D.SCALE;
				this.body.SetPositionAndAngle(new Box2D.Common.Math.b2Vec2(this.ss_posX, this.ss_posY), 0);

				// Check distance between ball and gem
				var distX 			= this.pos.x - this.ss_targetGem.pos.x;
				var distY 			= this.pos.y - this.ss_targetGem.pos.y;
				var distance 			= Math.sqrt(distX*distX + distY*distY);
				if(distance <= this.ss_distReq)
					this.ss_end();
			}
		},

		ss_end: function(){
			this.ss_isFiring = false;
			this.ss_willFire = false;

			var speed = 1,
				angle = this.ss_angle,
				forceX = speed * Math.cos((angle * Math.PI)/180),
				forceY = speed * Math.sin((angle * Math.PI)/180);
				force = new Box2D.Common.Math.b2Vec2(-forceX,-forceY);
			this.body.ApplyImpulse(force, this.body.GetPosition());
		}

	});


	/*
	  _______        _ _
	 |__   __|      (_) |
		| |_ __ __ _ _| |
		| | '__/ _` | | |
		| | | | (_| | | |
		|_|_|  \__,_|_|_|


		BALL TRAIL EFFECT
	*/
	EntityTrail = ig.Entity.extend({
		// Sprite properties
		imgSheet: new ig.Image('media/graphics/game/balltrail.png', 21, 21),
		size: {x:21, y:21},
		imgScale: 1, imgScale_red: 0.05,
		zIndex: 2,

		duration: 1,

		init: function(x,y,settings){
			this.parent(x,y,settings);
		},

		draw: function(){
			var ctx = ig.system.context;
			ctx.save();

			ctx.drawImage(	this.imgSheet.data, 0, 0,
							this.imgSheet.width,
							this.imgSheet.height,
							this.pos.x + (this.imgSheet.width / 2) * (1-this.imgScale),
							this.pos.y + (this.imgSheet.height / 2) * (1-this.imgScale),
							this.imgSheet.width * this.imgScale,
							this.imgSheet.height * this.imgScale  );

			ctx.restore();
		},

		update: function(){
			this.duration -= ig.system.tick;
			this.imgScale -= this.imgScale_red;

			if(this.duration <= 0 || this.imgScale <= 0){
				this.kill();
			}
		}
	});
});
