ig.module('game.entities.objects.gem')
.requires(
	'plugins.box2d.entity'
)
.defines(function() {

	// MAIN ENTITY
	EntityGem = ig.Box2DEntity.extend({
		name: 'gem',

		// Physics - Box2D
		box2dType: 2,
		dynamicType: 0, // entity collision type 0 dynamice , 1 kinematic , 2 static
		density: 0.000001,
		friction: 0,
		restitution: 0, //entiy restitution
		gravityFactor: 0,

		// Sprite animation
		curScale: 0,
		curInc: 0.1,
		zIndex: 2,
		vertices: [{x: -2.81, y: -2.61},{x: 2.92, y: -2.66},{x: 3.02, y: 3.07},{x: -2.81, y: 2.92}],

		tutorialGem: false,
		isTaken: false,

		init: function(x,y,settings){
			if(ig.game.getEntityByName('gem')){
				this.name = 'gem2';
			}

			this.parent(x,y,settings);
			//this.body.SetGravityScale(0);
		},

		draw: function(){
			var ctx = ig.system.context;
			ctx.save();

			if(this.posLock >= 5)
				ctx.drawImage(	this.imgSheet.data, 0, 0,
								this.imgSheet.width,
								this.imgSheet.height,
								this.pos.x + (this.imgSheet.width / 2) * (1-this.curScale),
								this.pos.y + (this.imgSheet.height / 2) * (1-this.curScale),
								this.imgSheet.width * this.curScale,
								this.imgSheet.height * this.curScale  );

			ctx.restore();
		},

		isReady: false,
		posX: 0, posY: 0,
		posLock: 0,
		update: function(){
			// Reposition after spawn
			if(!this.isReady){
				var newPos = this._reposition();
				this.isReady = true;

				this.posX = newPos.x; this.posY = newPos.y;
			}
			else{
				//this.body.SetPositionAndAngle(new Box2D.Common.Math.b2Vec2(this.posX, this.posY), 0);

				this.body.SetAngle(0);
				if(this.posLock <= 15 && !ig.game.box2dPaused)	this.posLock++;
			}

			// Sprite scale
			if(this.curScale < 1 && this.posLock >= 5){
				this.curScale += this.curInc;

				if(this.curScale > 1)	this.curScale = 1;
			}

			// Reposition if outside the game area
			if(this.pos.x < 0 || this.pos.x > 540 || this.pos.y < 0 || this.pos.y > 960){
				var newPos = this._reposition();
				this.posX = newPos.x; this.posY = newPos.y;
				this.posLock = 0;
			}
			if(this.repositioning){
				this.body.SetPositionAndAngle(new Box2D.Common.Math.b2Vec2(this.posX, this.posY), 0);
				this.repositioning = false;
			}

			// Special systems
			/*Magnet*/			this.mag_update();

			this.parent();
		},

		/*
		   _____      _ _ _     _
		  / ____|    | | (_)   (_)
		 | |     ___ | | |_ ___ _  ___  _ __
		 | |    / _ \| | | / __| |/ _ \| '_ \
		 | |___| (_) | | | \__ \ | (_) | | | |
		  \_____\___/|_|_|_|___/_|\___/|_| |_|

			COLLISION TO WALLS / OBSTACLES
		*/
		beginContact: function (other, contact) {
			// if(other.name && this.body)
			// 	if(other.name != 'ball' && !this.mag_isPulled && this.posLock <= 15){
			// 		this.body.SetPosition(new Box2D.Common.Math.b2Vec2(-500, -500));
			// 		this.isReady = false;
			// 	}

		},

		animNull: false, repositioning: false,
		_reposition: function(){
			if(this.mag_isPulled){
				return {x:this.posX, y:this.posY};
			}

			// Respawn if inside obstacle
			var minX = 7, maxX = 45, minY = 20, maxY = 80;
			if(this.tutorialGem){
				var	randX = 220 * Box2D.SCALE,
					randY = 300 * Box2D.SCALE;
			}else{
				var	randX = Math.floor( Math.random() * (maxX - minX)) + minX,
					randY = Math.floor( Math.random() * (maxY - minY)) + minY;
			}

			if(this.body)
				this.body.SetPositionAndAngle(new Box2D.Common.Math.b2Vec2(randX, randY), 0);
			this.currentAnim = null;

			this.repositioning = true;
			return {x:randX, y:randY};
		},

		preSolve: function(other, contact){
			if(other.name === 'ball' || this.mag_isPulled){
				contact.SetEnabled(false);
				return;
			}

			if(other.name)
				if((other.name.indexOf('obstacle')!==-1 || other.name == 'bouncer')){
					if(this.mag_isPulled){
						contact.SetEnabled(false);
					}
				}

			if(other.name && this.body)
				if((other.name.indexOf('obstacle')!==-1 || other.name == 'bouncer') && this.posLock <= 7){
					var newPos = this._reposition();
					this.isReady = true;

					this.posX = newPos.x; this.posY = newPos.y;
					this.posLock++;
					if(this.posLock >= 7){
						var randPos = Math.random();
						if(randPos <= 0.1)				{this.posX = 246; this.posY = 499;}
						else if(randPos <= 0.2)			{this.posX = 370; this.posY = 389;}
						else if(randPos <= 0.3)			{this.posX = 330; this.posY = 225;}
						else if(randPos <= 0.4)			{this.posX = 176; this.posY = 208;}
						else if(randPos <= 0.5)			{this.posX = 138; this.posY = 393;}
						else if(randPos <= 0.6)			{this.posX = 194; this.posY = 589;}
						else if(randPos <= 0.7)			{this.posX = 303; this.posY = 599;}
						else if(randPos <= 0.8)			{this.posX = 194; this.posY = 688;}
						else if(randPos <= 0.9)			{this.posX = 319; this.posY = 695;}
						else							{this.posX = 258; this.posY = 652;}
					}
				}
		},

		postSolve: function(other, contact){

		},

		/*
		  __  __                        _
		 |  \/  |                      | |
		 | \  / | __ _  __ _ _ __   ___| |_
		 | |\/| |/ _` |/ _` | '_ \ / _ \ __|
		 | |  | | (_| | (_| | | | |  __/ |_
		 |_|  |_|\__,_|\__, |_| |_|\___|\__|
					 __/ |
				     |___/
			MAGNET SYSTEM
		*/
		mag_isPulled: false,
		mag_ball: null,
		mag_pullSpeed: 22,

		mag_update: function(){
			if(!this.mag_isPulled || ig.game.box2dPaused)		return;

			if(this.mag_ball === null){
				this.mag_ball = ig.game.getEntitiesByType(EntityBall)[0];
			}

			var angle = Math.atan2(	this.pos.y - this.mag_ball.pos.y,
									this.pos.x - this.mag_ball.pos.x ) * 180 / Math.PI,
				newPosX = this.mag_pullSpeed * Math.cos((angle * Math.PI)/180),
				newPosY = this.mag_pullSpeed * Math.sin((angle * Math.PI)/180);
			this.posX -= newPosX * Box2D.SCALE;
			this.posY -= newPosY * Box2D.SCALE;

			this.body.SetPositionAndAngle(new Box2D.Common.Math.b2Vec2(this.posX, this.posY), 0);
		}
	});

	/*
	  ______       _   _ _   _
	 |  ____|     | | (_) | (_)
	 | |__   _ __ | |_ _| |_ _  ___  ___
	 |  __| | '_ \| __| | __| |/ _ \/ __|
	 | |____| | | | |_| | |_| |  __/\__ \
	 |______|_| |_|\__|_|\__|_|\___||___/

		ENTITY VARIANTS
	*/
	EntityGem1 = EntityGem.extend({
		imgSheet: new ig.Image('media/graphics/game/gem/gem1.png', 42, 34),
		size:{x:42, y:34},
		//vertices: [{x: -2.10, y: -0.43},{x: -2.09, y: -0.82},{x: -1.33, y: -1.65},{x: 1.34, y: -1.65},{x: 2.07, y: -0.82},{x: 2.10, y: -0.39},{x: 0.00, y: 1.68}],
	});

	EntityGem2 = EntityGem.extend({
		imgSheet: new ig.Image('media/graphics/game/gem/gem2.png', 29, 48),
		size:{x:29, y:48},
		//vertices: [{x: -1.42, y: -1.55},{x: 0.00, y: -2.36},{x: 1.45, y: -1.53},{x: 1.45, y: 1.48},{x: 0.01, y: 2.40},{x: -1.38, y: 1.48}],
	});

	EntityGem3 = EntityGem.extend({
		imgSheet: new ig.Image('media/graphics/game/gem/gem3.png', 39, 44),
		size:{x:39, y:44},
		//vertices: [{x: -2.05, y: -0.08},{x: -1.37, y: -1.56},{x: 0.03, y: -2.25},{x: 1.25, y: -1.68},{x: 2.03, y: -0.13},{x: 1.33, y: 1.53},{x: 0.04, y: 2.23},{x: -1.33, y: 1.65}],
	});

	EntityGem4 = EntityGem.extend({
		imgSheet: new ig.Image('media/graphics/game/gem/gem4.png', 41, 46),
		size:{x:41, y:46},
		//vertices: [{x: -2.02, y: 0.11},{x: -2.00, y: -0.25},{x: 0.03, y: -2.37},{x: 2.01, y: -0.26},{x: 1.98, y: 0.21},{x: 0.00, y: 2.25}],
	});

	EntityGem5 = EntityGem.extend({
		imgSheet: new ig.Image('media/graphics/game/gem/gem5.png', 40, 39),
		size:{x:40, y:39},
		//vertices: [{x: -0.02, y: -1.97},{x: 1.92, y: 1.47},{x: 1.93, y: 1.88},{x: -2.03, y: 1.90},{x: -2.02, y: 1.49}],
	});
});
