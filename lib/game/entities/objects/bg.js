ig.module('game.entities.objects.bg')
.requires(
	'plugins.box2d.entity'
)
.defines(function() {
	EntityBg = ig.Entity.extend({
          zIndex: -1,

          init: function(x,y,settings){
               this.parent(x,y,settings);
          },

          draw: function(){
               var ctx = ig.system.context;
               ctx.save();

               // Rectangle background
               ctx.beginPath();
               ctx.rect(this.pos.x, this.pos.y, ig.system.width, ig.system.height);
               ctx.fillStyle = '#361d4b';
               ctx.fill();

               ctx.restore();
          }
     });
});
