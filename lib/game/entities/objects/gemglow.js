ig.module('game.entities.objects.gemglow')
.requires(
	'plugins.box2d.entity'
)
.defines(function() {
	EntityGemGlow = ig.Entity.extend({
          imgSheet: new ig.Image('media/graphics/game/menu/gemglow.png', 23, 24),
          zIndex: 1,

          // Sprite animation
          curScale: 0,
          curInc: 0.03,
          curDec: false,
          maxSize: 1,

          init: function(x,y,settings){
			this.parent(x,y,settings);

               this.maxSize = (Math.random() * 0.8) + 1;
		},

          draw: function(){
			var ctx = ig.system.context;
			ctx.save();

			ctx.drawImage(	this.imgSheet.data, 0, 0,
							this.imgSheet.width,
							this.imgSheet.height,
							this.pos.x + (this.imgSheet.width / 2) * (1-this.curScale),
							this.pos.y + (this.imgSheet.height / 2) * (1-this.curScale),
							this.imgSheet.width * this.curScale,
							this.imgSheet.height * this.curScale  );

			ctx.restore();
		},

          update: function(){
               // Sprite scale
			if(!this.curDec){
                    this.curScale += this.curInc;
                    if(this.curScale > 1)    this.curDec = true;
               }else{
                    this.curScale -= this.curInc;
                    if(this.curScale < 0)    this.kill();
               }
          }
     });
});
