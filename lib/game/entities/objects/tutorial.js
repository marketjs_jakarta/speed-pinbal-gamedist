ig.module('game.entities.objects.tutorial')
.requires(
	'plugins.box2d.entity'
)
.defines(function() {
	EntityTutorial = ig.Entity.extend({
          text1: '', text2: '',
          tutNum: 1,
		zIndex: 2000,

          init: function(x,y,settings){
			this.parent(x,y,settings);

			switch(settings.tutNum){
				// Texts with mobile/dekstop dependency
				case 1: case 2: case 8:
					if(ig.ua.mobile){
						this.text1 = _STRINGS["Tutorial"]["tut" + settings.tutNum + "_1mob"];
						this.text2 = _STRINGS["Tutorial"]["tut" + settings.tutNum + "_2mob"];
					}else{
						this.text1 = _STRINGS["Tutorial"]["tut" + settings.tutNum + "_1desk"];
						this.text2 = _STRINGS["Tutorial"]["tut" + settings.tutNum + "_2desk"];
					}
				break;
				// Texts with no mobile/dekstop dependency
				default:
					this.text1 = _STRINGS["Tutorial"]["tut" + settings.tutNum + "_1"];
					this.text2 = _STRINGS["Tutorial"]["tut" + settings.tutNum + "_2"];
			}

			// Special tutorial events
			var gameControl = ig.game.getEntitiesByType(EntityGameControl)[0];
			if(settings.tutNum == 3){
				gameControl.gemToSpawn = 1;
				gameControl.tutorialGem = true;
			}
			else if(settings.tutNum == 5){
				ig.game.spawnEntity(EntityMagnet, 0, 0, {tutorial:true});
				gameControl.tutorialGem = false;
			}
		},

          update: function(){

          },

          draw: function(){
               var w, h;
			var 	ctx = ig.system.context,
				str1 = this.text1, str2 = this.text2;

			w = ctx.measureText(str1).width;
			h = 0;

			ctx.save();

			// Rectangular background
			ctx.beginPath();
			ctx.globalAlpha = 0.3;
			ctx.rect(0, 410, ig.system.width, 100);
			ctx.fillStyle = '#ff0000';
			ctx.fill();

			// Texts
			ctx.font = '25px Somatic Rounded, Helvetica, Verdana';
			ctx.globalAlpha = 1;
			ctx.fillStyle = '#aed6f3';
			ctx.textAlign = "center";
			ctx.fillText(str1, this.pos.x, this.pos.y);
			ctx.fillText(str2, this.pos.x, this.pos.y + 35);

			ctx.restore();
          },

          /*
               NEXT TUTORIAL / DESTROY TUTORIAL
          */
          _destroy: function(){
			var gameControl = ig.game.getEntitiesByType(EntityGameControl)[0];
               switch(this.tutNum){
                    case 1: case 2: case 3: case 4: case 5: case 6: case 7:
                         gameControl.spawnTut = true;
                         gameControl.nextTut = this.tutNum + 1;
                    break;
				default:
					// TUTORIAL ENDS
					gameControl.tutorial = null;
					gameControl.sc_start();
					gameControl.uiControl.btnPause.isEnabled = true;

					ig.game.getEntitiesByType(EntityGem)[0].kill();
					ig.game.getEntitiesByType(EntityMagnet)[0].kill();
					// gameControl.gemToSpawn = 1;

					ig.game.tutorial = false;
               }

               this.kill();
          }
     });
});
