ig.module('game.entities.objects.test-border')
.requires(
	'plugins.box2d.entity'
)
.defines(function() {
	EntityTestBorder = ig.Box2DEntity.extend({

		name: 'obstacle1',

		// Sprite properties
		animSheet: new ig.AnimationSheet('media/graphics/game/border.png', 540, 960),
		size: {x:540, y:960},

		// Physics - Box2D
		box2dType: 2,
		dynamicType: 2, // entity collision type 0 dynamice , 1 kinematic , 2 static
		density: 0.1,
		friction: 1,
		restitution: 0, //entiy restitution
		vertices: [{x: -24.66, y: 33.77},{x: -13.43, y: 38.56},{x: -13.48, y: 43.01},{x: -24.71, y: 43.16}],

		init:function(x,y,settings){
			this.parent(x,y,settings);

			this.addAnim('idle', 1, [0]);

			if (ig.global.wm) return;

			this.createParts();
		},

		beginContact: function (other) { //console.log('contact!' + other.name);
			if((other.name === 'gem' || other.name === 'gem2' || other.name === 'magnet') && other.body && !other.mag_isPulled){
				if(other.posLock <= 14){
					//other.isReady = false;
					//other.posLock = 0;
					// console.log('colliding with obstacle, repositioning');
					// var newPos = other._reposition();
					// other.isReady = true;
                         //
					// other.posX = newPos.x; other.posY = newPos.y;
				}
			}
		},

		preSolve: function(other){

		},

		postSolve: function(other, contact){

		},

		/*
			VERTICES
		*/
		parts: [],
		createParts: function(){
			ig.game.spawnEntity(EntityTestBorder2, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9 );

			ig.game.spawnEntity(EntityTestBorder3, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9 );

			ig.game.spawnEntity(EntityTestBorder4, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9 );

			ig.game.spawnEntity(EntityTestBorder5, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9 );

			ig.game.spawnEntity(EntityTestBorder6, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9 );

			// ig.game.spawnEntity(EntityTestBorder7, 	this.pos.x + this.size.x / 2 - 8,
													// this.pos.y + this.size.y / 2 - 9 );

			ig.game.spawnEntity(EntityTestBorder8, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9 );

			ig.game.spawnEntity(EntityTestBorder9, 	this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9 );

			ig.game.spawnEntity(EntityTestBorder10, this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9 );

			ig.game.spawnEntity(EntityTestBorder11, this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9 );

			ig.game.spawnEntity(EntityTestBorder12, this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9 );

			ig.game.spawnEntity(EntityTestBorder13, this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9 );

			ig.game.spawnEntity(EntityTestBorder14, this.pos.x + this.size.x / 2 - 8,
													this.pos.y + this.size.y / 2 - 9 );
		}
	});

	EntityTestBorderParts = ig.Box2DEntity.extend({
		// Physics - Box2D
		box2dType: 2,
		dynamicType: 2, // entity collision type 0 dynamice , 1 kinematic , 2 static
		density: 0.1,
		friction: 1,
		restitution: 0, //entiy restitution

		init:function(x,y,settings){
			this.parent(x,y,settings);
		},

		beginContact: function (other) { //console.log('contact!' + other.name);
			if(other.name === 'gem'){
				//other._reposition();
			}
		}
	});

	EntityTestBorder2 = EntityTestBorderParts.extend({
		name: 'obstacle2',

		//vertices: [{x: -24.76, y: -43.11},{x: -22.74, y: -43.16},{x: -22.90, y: 44.69},{x: -24.86, y: 44.69}]
		vertices: [{x: -24.76, y: -39.14},{x: -22.74, y: -39.14},{x: -22.64, y: 43.21},{x: -24.61, y: 43.11}],
	});
	EntityTestBorder3 = EntityTestBorderParts.extend({
		name: 'obstacle3',

		//vertices:[{x: -22.74, y: -43.16}, {x: -20.69, y: -43.11}, {x: -21.04, y: -27.00}, {x: -22.90, y: -20.43}]
		//vertices: [{x: -22.11, y: -39.09},{x: -19.74, y: -39.14},{x: -20.45, y: -23.69},{x: -22.77, y: -15.95}],
		vertices: [{x: -22.67, y: -39.24},{x: -19.94, y: -39.14},{x: -20.50, y: -23.69},{x: -22.72, y: -16.51}],
	});
	EntityTestBorder4 = EntityTestBorderParts.extend({
		name: 'obstacle4',

		//vertices: [{x: -20.69, y: -43.11},{x: -16.84, y: -43.11},{x: -16.99, y: -32.80},{x: -21.04, y: -27.00}]
		vertices: [{x: -19.74, y: -39.14}, {x: -15.54, y: -39.09}, {x: -15.90, y: -29.95}, {x: -20.45, y: -23.69}],
	});
	EntityTestBorder5 = EntityTestBorderParts.extend({
		name: 'obstacle5',

		//vertices: [{x: -16.84, y: -43.11},{x: -11.67, y: -43.16},{x: -11.93, y: -36.82},{x: -16.99, y: -32.80}]
		vertices: [{x: -15.54, y: -39.09},{x: -8.69, y: -39.19},{x: -9.00, y: -34.38},{x: -15.90, y: -29.95}],
	});
	EntityTestBorder6 = EntityTestBorderParts.extend({
		name: 'obstacle6',

		//vertices: [{x: -11.67, y: -43.16},{x: -4.31, y: -43.11},{x: -4.41, y: -39.62},{x: -11.93, y: -36.82}]
		vertices: [{x: -8.69, y: -39.19}, {x: -1.85, y: -39.14}, {x: -1.59, y: -36.06}, {x: -9.00, y: -34.38}],
	});
	EntityTestBorder7 = EntityTestBorderParts.extend({
		name: 'obstacle7',

		//vertices: [{x: -4.31, y: -43.11},{x: -0.32, y: -43.11},{x: -0.47, y: -39.78},{x: -4.41, y: -39.62}]
		//vertices: [{x: -1.85, y: -39.14},{x: -8.69, y: -39.19},{x: -9.00, y: -34.38},{x: -1.59, y: -36.06}],
	});
	EntityTestBorder8 = EntityTestBorderParts.extend({
		name: 'obstacle8',

		//vertices: [{x: -0.32, y: -43.11},{x: 5.31, y: -43.06},{x: 5.21, y: -39.22},{x: -0.47, y: -39.78}]
		vertices: [{x: -1.85, y: -39.14},{x: 6.33, y: -39.04},{x: 6.33, y: -35.19},{x: -1.59, y: -36.06}],
	});
	EntityTestBorder9 = EntityTestBorderParts.extend({
		name: 'obstacle9',

		//vertices: [{x: 5.51, y: -43.21}, {x: 11.95, y: -43.11}, {x: 11.85, y: -36.77}, {x: 5.26, y: -39.22}]
		vertices: [{x: 13.68, y: -39.19}, {x: 13.53, y: -31.73}, {x: 6.33, y: -35.19}, {x: 6.33, y: -39.04}],
	});
	EntityTestBorder10 = EntityTestBorderParts.extend({
		name: 'obstacle10',

		//vertices: [{x: 11.95, y: -43.11},{x: 17.28, y: -43.16},{x: 17.38, y: -32.14},{x: 11.85, y: -36.77}]
		vertices: [{x: 18.75, y: -39.24}, {x: 18.55, y: -26.84}, {x: 13.53, y: -31.73}, {x: 13.68, y: -39.19}],
	});
	EntityTestBorder11 = EntityTestBorderParts.extend({
		name: 'obstacle11',

		//vertices: [{x: 17.28, y: -43.16}, {x: 20.86, y: -43.16}, {x: 21.11, y: -26.39}, {x: 17.38, y: -32.14}]
		vertices: [{x: 18.75, y: -39.24}, {x: 21.37, y: -39.14}, {x: 21.62, y: -20.89}, {x: 18.55, y: -26.84}],
	});
	EntityTestBorder12 = EntityTestBorderParts.extend({
		name: 'obstacle12',

		//vertices: [{x: 20.86, y: -43.16},{x: 23.03, y: -43.11},{x: 22.77, y: -19.56},{x: 21.11, y: -26.39}]
		vertices: [{x: 21.22, y: -39.14},{x: 22.98, y: -39.14},{x: 22.57, y: -14.98},{x: 21.62, y: -20.89}],
	});
	EntityTestBorder13 = EntityTestBorderParts.extend({
		name: 'obstacle13',

		//vertices: [{x: 22.37, y: -43.01}, {x: 24.47, y: -42.96}, {x: 24.47, y: 44.79}, {x: 22.67, y: 44.84}]
		vertices: [{x: 22.49, y: -39.24},{x: 24.56, y: -39.14},{x: 24.56, y: 43.11},{x: 22.64, y: 43.21}],
	});
	EntityTestBorder14 = EntityTestBorderParts.extend({
		name: 'obstacle13',

		//vertices: [{x: 24.42, y: 36.52}, {x: 24.47, y: 44.79}, {x: 14.58, y: 44.64}, {x: 14.58, y: 40.59}]
		vertices: [{x: 13.79, y: 38.30},{x: 24.64, y: 33.98},{x: 24.82, y: 43.21},{x: 13.61, y: 43.06}],
	});

	//
});
