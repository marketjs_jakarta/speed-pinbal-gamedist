ig.module('game.entities.controllers.game-control')
.requires(
	'impact.entity',
	'game.entities.controllers.game-control-ui'
)
.defines(function() {
	EntityGameControl = ig.Entity.extend({

		name: 'gameControl',

		// Entities
		ball: null,
		uiControl: null, obsControl: null,
		leftPaddle: null, rightPaddle: null,
		pointer: null,

		// Bonus ball
		bonusBall: false, bonusBallExist: false,
		bonusGem: false,

		init:function(x,y,settings){
			if (ig.global.wm) return;

			try{
				ig.multitouchInput = new ig.MultitouchInput();
				this.multitouch = new Multitouch();
			}catch(e){
				console.log(e);
			}

			// Reset globals
			ig.game.curGems = 0;
			ig.game.curBalls = ig.game.upg_Ball + 1;
			ig.game.box2dPaused = false;

			// Entities
			this.uiControl = ig.game.spawnEntity(EntityGameControlUi, 		0, 0, {gameControl:this});
			this.obsControl = ig.game.spawnEntity(EntityObsControl, 		0, 0, {gameControl:this});
			this.leftPaddle = ig.game.spawnEntity(EntityPaddle, 		 	143, 858, {gameControl:this});	// 140, 860
			this.rightPaddle = ig.game.spawnEntity(EntityPaddleRight,		386, 858, {gameControl:this});	// 383, 860
			if(ig.game.upg_spring > 0)
				ig.game.spawnEntity(EntitySpring, 						215, 915, {gameControl:this});	// 217, 930

			// Other values
			this.ss_intervalMax = 60 - 10 * ig.game.upg_sureShot;
			//this.ss_intervalMax = 1;

			// Game State
			ig.game.gameState = 'in game';

			// Other components
			/*Magnet countdown*/			this.mag_start();

			this.parent(x,y,settings);
		},

		isReady: false,
		ready:function(){
			if (ig.global.wm) return;

			this.pointer = ig.game.getEntitiesByType(EntityPointer)[0];

			// Other components
			/*Start countdown*/				this.sc_start();
			// If tutorial runs, spawn ball
			if(this.tut_init()){
				this._spawnBall();
			}

			this.isReady = true;
		},

		inputDelay: 0.9,
		update: function() {
			if(!this.isReady)	return;

			if(this.inputDelay > 0 && ig.game.tutorial){
				this.inputDelay -= ig.system.tick;
				return;
			}

			// Special systems
			/*Input*/						this._checkInputs();
			/*Gem Spawn*/					this._spawnGem_Update();
			/*Ball Spawn*/					this._spawnBall_Update();
			/*Sure Shot*/					this._ss_update();
			/*Check countdown*/				this._end_Countdown();
			/*Magnet countdown*/			this.mag_update();
			/*Start countdown*/				this.sc_update();
			/*Game over countdown*/			this._update_cDown_gameOver();
			/*Tutorial check spawn*/			this.tut_update();

			this.parent();
		},
		/*
	  	 _______    _             _       _
		|__   __|  | |           (_)     | |
		   | |_   _| |_ ___  _ __ _  __ _| |
		   | | | | | __/ _ \| '__| |/ _` | |
		   | | |_| | || (_) | |  | | (_| | |
		   |_|\__,_|\__\___/|_|  |_|\__,_|_|

			TUTORIAL
		*/
		tutorial: null,
		spawnTut: false, nextTut: 0,

		tut_init: function(){
			if(!ig.game.tutorial)	return false;
			var tutSpawn = {x:270, y:444};

			this.tutorial = ig.game.spawnEntity(EntityTutorial, tutSpawn.x, tutSpawn.y, {tutNum: 1});
			this._pause(true);
			this.uiControl.btnPause.isEnabled = false;
			this.sc_lastSec = 3;
			return true;
		},

		tut_update: function(){
			var tutSpawn = {x:270, y:444};

			if(this.spawnTut){
				this.tutorial = ig.game.spawnEntity(EntityTutorial, tutSpawn.x, tutSpawn.y, {tutNum: this.nextTut});
				this.spawnTut = false;
			}
		},
		/*
		  _____                   _
		 |_   _|                 | |
		   | |  _ __  _ __  _   _| |_
		   | | | '_ \| '_ \| | | | __|
		  _| |_| | | | |_) | |_| | |_
		 |_____|_| |_| .__/ \__,_|\__|
				   | |
				   |_|
			INPUT
		*/
		getTouchesPos:function(){
			return this.multitouch.getTouchesPos();
		},

		clicking: false,
		inputs: null,
		_checkInputs: function(){
			if(ig.game.gameState != 'in game')	return;
			if(this.tutorial){
				if(ig.input.pressed('click') || ig.input.pressed('Left') || ig.input.pressed('Right')){
					if(!this.uiControl.btnPause.checkMousePos())
						this.tutorial._destroy();
				}
				return;
			}

			if(ig.ua.mobile){
				// Mobile Multitouch Input
				if(this.getTouchesPos() != null){
					this.inputs = this.getTouchesPos();
				}else{
					this.inputs = null;
				}
				var hasLeftInput = false, hasRightInput = false;
				if(this.inputs != null){
					for(var i = 0; i < this.inputs.length; i++){
						if(this.inputs[i].x <= 270)	hasLeftInput = true;
						if(this.inputs[i].x > 270)	hasRightInput = true;
					}
				}

				this.leftPaddle.touchInput = hasLeftInput;
				this.rightPaddle.touchInput = hasRightInput;
			}else{
				// Old Mobile Input
				if(ig.input.pressed('click')){
					if(this.uiControl.btnPause.checkMousePos())	return;

					if(this.pointer.pos.x <= 270){
						this.leftPaddle.touchInput = true;
					}else{
						this.rightPaddle.touchInput = true;
					}
				}

				if(ig.input.released('click')){
					this.leftPaddle.touchInput = false;
					this.rightPaddle.touchInput = false;
				}
			}
		},

		/*
		  ____        _ _     _____            _             _
		 |  _ \      | | |   / ____|          | |           | |
		 | |_) | __ _| | |  | |     ___  _ __ | |_ _ __ ___ | |
		 |  _ < / _` | | |  | |    / _ \| '_ \| __| '__/ _ \| |
		 | |_) | (_| | | |  | |___| (_) | | | | |_| | | (_) | |
		 |____/ \__,_|_|_|   \_____\___/|_| |_|\__|_|  \___/|_|

			BALL CONTROL
		*/
		ballToSpawn: 0,
		firstBall: true,
		_spawnBall: function(){
			this.ballToSpawn++;
		},
		_spawnBall_Update: function(){
			while(this.ballToSpawn > 0){
				if(ig.game.curBalls > 0 || this.bonusBall){
					this.ball = ig.game.spawnEntity(EntityBall, 272, 100, {mag_duration: this.mag_duration});
					if(!this.bonusBall){
						ig.game.curBalls--;
					}else{
						this.bonusBallExist = true;
						this.bonusBall = false;
					}
					if(!this.firstBall){
						if(!this.bonusBall) this.uiControl._announce(_STRINGS["Announcer"]["extra ball"], '#04ca58', 2);
						ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.ballLaunch);
					}else this.firstBall = false;
				}
				else{
					///////////// OUT OF BALLS
					this._pause(true);
					this.gameOver = true;
					this._init_cDown_gameOver();
					this.uiControl._announce(_STRINGS["Announcer"]["out of balls"], '#ffffff', 2);
				}
				this.ballToSpawn--;
			}
		},

		/*
		   _____                        _____            _             _
		  / ____|                      / ____|          | |           | |
		 | |  __  ___ _ __ ___  ___   | |     ___  _ __ | |_ _ __ ___ | |
		 | | |_ |/ _ \ '_ ` _ \/ __|  | |    / _ \| '_ \| __| '__/ _ \| |
		 | |__| |  __/ | | | | \__ \  | |___| (_) | | | | |_| | | (_) | |
		  \_____|\___|_| |_| |_|___/   \_____\___/|_| |_|\__|_|  \___/|_|

			GEMS CONTROL
		*/
		gemToSpawn: 0,
		tutorialGem: false,

		_spawnGem: function(){
			if(this.gemToSpawn < 2)
				this.gemToSpawn++;

			//Reset countdown
			if(this.cDown)			this.cDown.set(this.maxTime);
		},
		_spawnGem_Update: function(){
			// Random gem
			while(this.gemToSpawn > 0){
				var gemType = Math.random();
				if(gemType <= 0.2) 				ig.game.spawnEntity(EntityGem1, 0, 0, {tutorialGem:this.tutorialGem});
				else if(gemType <= 0.4) 			ig.game.spawnEntity(EntityGem2, 0, 0, {tutorialGem:this.tutorialGem});
				else if(gemType <= 0.6) 			ig.game.spawnEntity(EntityGem3, 0, 0, {tutorialGem:this.tutorialGem});
				else if(gemType <= 0.8) 			ig.game.spawnEntity(EntityGem4, 0, 0, {tutorialGem:this.tutorialGem});
				else 						ig.game.spawnEntity(EntityGem5, 0, 0, {tutorialGem:this.tutorialGem});

				this.gemToSpawn--;
			}
		},

		/*
			_____                  _      _
		    / ____|                | |    | |
		   | |     ___  _   _ _ __ | |_ __| | _____      ___ __  ___
		   | |    / _ \| | | | '_ \| __/ _` |/ _ \ \ /\ / / '_ \/ __|
		   | |___| (_) | |_| | | | | || (_| | (_) \ V  V /| | | \__ \
		    \_____\___/ \__,_|_| |_|\__\__,_|\___/ \_/\_/ |_| |_|___/

			COUNTDOWN (START, MAIN GAME AND GAME OVER)
		*/
		////////////////// START / RESUME COUNTDOWN /////////////////
		sc: true,
		sc_timer: null,
		sc_lastSec: 4,
		sc_isResume: false,
		sc_start:function(){
			if(this.sc_isResume)			this.sc_timer = new ig.Timer(2);		// Resume from pause
			else 						this.sc_timer = new ig.Timer(3);		// Game start
			this.sc = true;
			this.sc_lastSec = 4;
		},
		sc_update:function(){
			if(!this.sc)	return;

			if(this.sc_lastSec > this.sc_timer.delta() * -1){
				this.sc_lastSec = parseInt(this.sc_timer.delta() * -1);
				this.uiControl._announce(this.sc_lastSec + 1, '#ffffff', 1);

				if(this.sc_timer.delta() < 0)
					ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.beep);
			}

			if(this.sc_timer.delta() >= 0){
				if(!this.sc_isResume)		this._gameStart();
				else 					this._gameResume();
			}
		},

		_gameStart:function(){
			///////////////// GAME START /////////////////
			/*Game countdown init*/				this._init_Countdown();
			/*Spawn ball and gem if not tutorial*/

			if(!ig.game.getEntitiesByType(EntityBall)[0]){
				// Spawn ball and gem if not tutorial
				this._spawnBall();
			}
			else
				ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.ballLaunch);
			this._spawnGem();


			this.uiControl._announce(_STRINGS["Announcer"]["go"], '#ffffff', 1);
			ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.go);
			this.sc = false;
			this.cDown.set(this.maxTime);
			ig.game.box2dPaused = false;
		},

		_gameResume:function(){
			this.sc_isResume = false;
			ig.game.box2dPaused = false;

			if(this.ball == null){
				this._gameStart();
				return;
			}

			if(this.cDown)
				this.cDown.unpause();
			else{
				this._init_Countdown();
				this._spawnGem();
			}
			this.mag_Timer.unpause();
			this.sc_timer.unpause();

			this.uiControl._announce(_STRINGS["Announcer"]["go"], '#ffffff', 1);
			ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.go);
			this.sc = false;
		},

		////////////////// GAME COUNTDOWN /////////////////
		cDown: null,
		_init_Countdown:function(){
			var cTime = 16 + ig.game.upg_countdown * 2;
			this.maxTime = cTime;
			this.cDown = new ig.Timer(cTime);
		},
		_end_Countdown:function(){
			if(!this.cDown)		return;

			///////////// COUNTDOWN TIMER RAN OUT
			if(this.cDown.delta() > 0 && !this.gameOver && !ig.game.box2dPaused){
				this._pause(true);
				this.gameOver = true;
				this._init_cDown_gameOver();
				this.uiControl._announce(_STRINGS["Announcer"]["time over"], '#ffffff', 2);
			}
		},

		////////////////// GAME OVER COUNTDOWN /////////////////
		cDown_gameOver: null,
		_init_cDown_gameOver:function(){
			this.cDown_gameOver = new ig.Timer(2);

			// Disable pause button
			var pauseBtn = ig.game.getEntitiesByType(EntityBtnGame)[0];
			pauseBtn.isEnabled = false;
			ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.gameOver);
		},

		gameOver2: false,
		_update_cDown_gameOver:function(){
			if(!this.cDown_gameOver || this.gameOver2)		return;
			if(this.cDown_gameOver.delta() < 0)			return;

			ig.game.spawnEntity(EntityGameover, 550, 0);
			//ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.transition);
			this.gameOver2 = true;
		},

		/*
		 __  __                        _
		|  \/  |                      | |
		| \  / | __ _  __ _ _ __   ___| |_
		| |\/| |/ _` |/ _` | '_ \ / _ \ __|
		| |  | | (_| | (_| | | | |  __/ |_
		|_|  |_|\__,_|\__, |_| |_|\___|\__|
				    __/ |
				   |___/

		MAGNET CONTROL
		*/
		mag_Timer: null,
		mag_TimeInc: 0,
		mag_duration: 0,
		mag_firstMag: true,

		mag_start:function(){
			//FOR TESTING: var magTime = 10000000000000;
			if(this.mag_firstMag){
				var magTime = 10;
				this.mag_firstMag = false;
			}else
				var magTime = (Math.random() * (25 - 10)) + 10 + this.mag_TimeInc;
			this.mag_Timer = new ig.Timer(magTime);
			//console.log('magnet spawns in ' + magTime + ' seconds');

			this.mag_TimeInc += 3.5;
		},

		mag_update:function(){
			if(this.mag_Timer.delta() > 0 && !ig.game.box2dPaused){
				var mag_powerup = ig.game.getEntitiesByType(EntityMagnet)[0];
				if(mag_powerup == null){
					this.mag_start();
				}else{
					return;
				}

				this.mag_start();
				ig.game.spawnEntity(EntityMagnet, 0, 0, {tutorial:this.tutorialGem});
			}
		},

		/*
		   _____                   _____ _           _
		  / ____|                 / ____| |         | |
		 | (___  _   _ _ __ ___  | (___ | |__   ___ | |_
		  \___ \| | | | '__/ _ \  \___ \| '_ \ / _ \| __|
		  ____) | |_| | | |  __/  ____) | | | | (_) | |_
		 |_____/ \__,_|_|  \___| |_____/|_| |_|\___/ \__|

		 SURE SHOT SYSTEM
		*/
		ss_interval: 0,
		ss_intervalMax: 0,
		ss_available: false,

		_ss_update: function(){
			if(ig.game.box2dPaused)						return;
			if(this.sc_timer) 	if(this.sc_timer.delta() < 0)	return;
			this.ss_interval += ig.system.tick;

			if(this.ss_interval >= this.ss_intervalMax){
				this.ss_interval = this.ss_intervalMax;

				// Activate sure shot
				if(!this.ss_available){
					this.ss_available = true;
					ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.sureShotReady);
				}
			}
			else{
				this.ss_available = false;
			}
		},

		/*
			_____
		    |  __ \
		    | |__) |_ _ _   _ ___  ___
		    |  ___/ _` | | | / __|/ _ \
		    | |  | (_| | |_| \__ \  __/
		    |_|   \__,_|\__,_|___/\___|

	    		PAUSE CONTROL
		*/
		gameOver: false,
		_pause: function(pause){
			if(pause){
				if(this.cDown) 		this.cDown.pause();
				if(this.sc_timer)		this.sc_timer.pause();
				if(this.mag_Timer)		this.mag_Timer.pause();
			}else{

				//if(this.sc_timer.delta() < 0)	this.sc_timer.unpause();
				this.mag_Timer.unpause();
			}

			ig.game.box2dPaused = pause;
		},

		_gamePause: function(pause){
			if(pause){
				var pauseWindow = ig.game.getEntitiesByType(EntitySettings)[0];
				if(pauseWindow == null){
					this._pause(true);
					ig.game.spawnEntity(EntitySettings, 550, 0, {isPause: true});
				}
			}
			else{
				this.sc_isResume = true;
				this.sc_start();		// Resume countdown
			}
		}
	});
});
