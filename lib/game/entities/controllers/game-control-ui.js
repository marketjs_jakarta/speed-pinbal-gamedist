ig.module('game.entities.controllers.game-control-ui')
.requires(
	'impact.entity'
)
.defines(function() {
	EntityGameControlUi = ig.Entity.extend({

		name: 'uiControl',
		gameControl: null,

		zIndex: 30,

		// Images
		img_iconTime 			: new ig.Image('media/graphics/game/ui/icontime.png', 38, 41),
		img_iconSureshot 		: new ig.Image('media/graphics/game/ui/iconsureshot.png', 38, 41),
		img_iconSureshotR 		: new ig.Image('media/graphics/game/ui/iconsureshotready.png', 50, 53),
		img_barBase 			: new ig.Image('media/graphics/game/ui/barbase.png', 120, 16),
		barSize: {x:110, y:10},
		img_bar 				: new ig.Image('media/graphics/game/ui/bar.png', 129, 12),
		img_gemui 			: new ig.Image('media/graphics/game/ui/gemui.png', 53, 38),

		btnPause: null,
		bg: null,

		init:function(x,y,settings){
			if (ig.global.wm) return;

			this.parent(x,y,settings);
			this.bg = ig.game.spawnEntity(EntityBg, 0, 0);

			this.btnPause = ig.game.spawnEntity(EntityBtnGame, 476, 8);
		},

		/*
		  _____
		 |  __ \
		 | |  | |_ __ __ ___      __
		 | |  | | '__/ _` \ \ /\ / /
		 | |__| | | | (_| |\ V  V /
		 |_____/|_|  \__,_| \_/\_/

		 DRAW FUNCTION
		*/
		lastSSbar: 0,
		draw:function(){
			var ctx = ig.system.context;
			ctx.save();

			// Gems
			this.img_gemui.draw(26,18);
			this._drawText(ig.game.curGems, 'Somatic Rounded', 33, 43, 24, '#FFFFFF');

			// Duration
			var timeDelta 	= (this.gameControl.cDown) ? this.gameControl.cDown.delta() * -1 : this.gameControl.maxTime,
				maxTime 	= this.gameControl.maxTime,
				durBar 	= this.barSize.x * (timeDelta / maxTime);

			this.img_barBase.draw(176,28);
			if(durBar > 0 && durBar <= this.barSize.x){
				ctx.drawImage(this.img_bar.data, 176, 31, durBar, this.barSize.y);
			}
			this.img_iconTime.draw(138,16);

			// Sure Shot Interval
			var ss_interval 	= this.gameControl.ss_interval,
				ss_intervalMax	= this.gameControl.ss_intervalMax,
				ss_percentage	= (ss_interval / ss_intervalMax),
				ss_bar		= this.barSize.x * ss_percentage;
			if(ss_bar > this.barSize.x)			ss_bar = this.barSize.x;
			if(ss_bar < 1 || isNaN(ss_bar))		ss_bar = 1;
			ss_bar = Math.floor(ss_bar);

			// Prevent maximum sure shot bar on start countdown
			if(this.gameControl.sc_timer){
				if(this.gameControl.sc_timer.delta() < 0 && !this.gameControl.sc_isResume)	ss_bar = 1;
			}

			if(this.lastSSbar < 1 && ss_percentage >= 1){
				// Ready indicator text
				// this._createMovingText( _STRINGS["Game"]["SureShotReady"], 'Nord', 32, 130, 30, 0.25, 1, 1.5, '#ffd700' );
				// /*Ready indicator text 2*/	this.ssReadyDur = 2.5;
				this._announce(_STRINGS["Announcer"]["sureShotReady"], '#8eff56', 1.5);
			}
			this.lastSSbar = ss_percentage;

			this.img_barBase.draw(356,28);
			if(ss_bar > 0 && ss_bar <= this.barSize.x){
				ctx.drawImage(this.img_bar.data, 356, 31, ss_bar, this.barSize.y);
			}
			if(this.lastSSbar < 1)
				this.img_iconSureshot.draw(318,16);
			else
				this.img_iconSureshotR.draw(314,10);
			ctx.restore();

			// Special systems
			/*Moving texts*/			this._mt_update();
			/*Announcement text*/		this._announce_draw();

			ig.game.sortEntitiesDeferred();
		},

		/*
		  _    _           _       _
		 | |  | |         | |     | |
		 | |  | |_ __   __| | __ _| |_ ___
		 | |  | | '_ \ / _` |/ _` | __/ _ \
		 | |__| | |_) | (_| | (_| | ||  __/
		  \____/| .__/ \__,_|\__,_|\__\___|
				| |
				|_|

			UPDATE FUNCTION
		*/

		update: function(){
			this.parent();

			// Special systems
			/*Announcement update*/		this._announce_update();
		},

		/*
		  _______        _
		 |__   __|      | |
			| | _____  _| |_ ___
			| |/ _ \ \/ / __/ __|
			| |  __/>  <| |_\__ \
			|_|\___/_/\_\\__|___/

			TEXTS AND MOVING TEXTS
		*/
		// Regular UI texts
		_drawText:function(str, font, fontSize, x, y, color){
			/*
				FONTS: Nord, Somatic Rounded
			*/
			// Variables
			var w, h;
			var ctx = ig.system.context;

			w = ctx.measureText(str).width;
			h = 0;

			ctx.save();
			ctx.font = fontSize + 'px ' + font + ', Helvetica, Verdana';
			ctx.globalAlpha = 1;

			ctx.fillStyle = color;
			ctx.translate(x, y);
			ctx.textAlign = "left";
			ctx.fillText(str, x, y);

			ctx.restore();
		},

		// Moving texts
		mt_array: [],
		toSplice: [],

		_createMovingText: function(str, font, fontSize, posX, posY, yIncrement, yIncDelay, duration, color){
			var newMt = {x:posX, y:posY, yInc:yIncrement, yDelay:0, yDelayMax:yIncDelay, dur:duration, txt:str, f:font, fs:fontSize, clr:color};

			this.mt_array.push(newMt);
		},

		_mt_update: function(){
			if(this.mt_array.length < 0)	return;

			for(var i = this.mt_array.length-1; i >= 0; i--){
				// Increase Y
				this.mt_array[i].yDelay++;
				if(this.mt_array[i].yDelay >= this.mt_array[i].yDelayMax){
					this.mt_array[i].y += this.mt_array[i].yInc;
					this.mt_array[i].yDelay = 0;
				}

				this._drawText(this.mt_array[i].txt, this.mt_array[i].f, this.mt_array[i].fs, this.mt_array[i].x, this.mt_array[i].y, this.mt_array[i].clr);

				// Duration
				this.mt_array[i].dur -= ig.system.tick;
				if(this.mt_array[i].dur <= 0){
					this.toSplice.push(i);
				}
			}

			// Splice texts with duration < 0
			for(var i = 0; i < this.toSplice.length; i++){
				this.mt_array.splice(this.toSplice[i], 1);
			}
			if(this.toSplice.length >= 0)
				this.toSplice = [];
		},
		/*
																	_
			    /\                                                               | |
			   /  \   _ __  _ __   ___  _   _ _ __   ___ ___ _ __ ___   ___ _ __ | |_
			  / /\ \ | '_ \| '_ \ / _ \| | | | '_ \ / __/ _ \ '_ ` _ \ / _ \ '_ \| __|
			 / ____ \| | | | | | | (_) | |_| | | | | (_|  __/ | | | | |  __/ | | | |_
			/_/    \_\_| |_|_| |_|\___/ \__,_|_| |_|\___\___|_| |_| |_|\___|_| |_|\__|
			TEXT ANNOUNCEMENT
		*/
		ann_text: '', ann_color: '#ffffff',
		ann_tween: null,
		ann_dur: 0, ann_durMax: 1.5,
		ann_enabled: false, ann_countdown: false,

		ann_curPos:	{x: 0, y: 0, alpha:0},
		ann_strtPos: 	{x: 270, y: 410, alpha:0},
		ann_endPos: 	{x: 270, y: 370, alpha:1},
		ann_endPos2: 	{x: 270, y: 330, alpha:0},

		_announce:function(str, color, dur){
			this.tweenAnn = null;

			this.ann_text 				= str;
			this.ann_color 			= color;
			this.ann_curPos.x 			= this.ann_strtPos.x;
			this.ann_curPos.y 			= this.ann_strtPos.y;
			this.ann_curPos.alpha 		= this.ann_strtPos.alpha;
			this.ann_enabled 			= true;
			this.ann_durMax 			= dur;
			this.ann_dur 				= this.ann_durMax;

			this._announce_tweenStart();
		},

		_announce_tweenStart:function(){
			var 	duration = 0.33,
				targetPos = (this.ann_dur > 0) ? this.ann_endPos : this.ann_endPos2;
			this.tweenAnn = this.tween({ann_curPos : targetPos}, duration, {
				entity:this,
				easing:ig.Tween.Easing.Quadratic.EaseInOut,
				onComplete: function(){
					this.entity.tweenAnn = null;
					this.entity._announce_tweenEnd();
				}
			});
			this.tweenAnn.start();
		},

		_announce_tweenEnd:function(){
			if(this.ann_dur > 0){
				this.ann_countdown = true;
			}
			else{
				this.ann_enabled = false;
			}
		},

		_announce_update:function(){
			if(!this.ann_enabled)	return;

			if(this.ann_dur > 0){
				if(this.ann_countdown) 		this.ann_dur -= ig.system.tick;
			}else{
				if(this.ann_countdown){
					this.ann_countdown = false;
					this._announce_tweenStart();
				}
			}
		},

		_announce_draw:function(){
			if(!this.ann_enabled)	return;

			var w, h;
			var 	ctx = ig.system.context,
				str = this.ann_text;

			w = ctx.measureText(str).width;
			h = 0;

			ctx.save();
			ctx.font = '50px Nord, Helvetica, Verdana';
			ctx.globalAlpha = this.ann_curPos.alpha;

			ctx.fillStyle = this.ann_color;
			ctx.textAlign = "center";
			ctx.fillText(str, this.ann_curPos.x, this.ann_curPos.y);

			ctx.restore();
		}
	});
});
