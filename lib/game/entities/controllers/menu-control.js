ig.module('game.entities.controllers.menu-control')
.requires(
	'impact.entity'
)
.defines(function() {
	EntityMenuControl = ig.Entity.extend({

		name: 'menuControl',
		imgBg: new ig.Image('media/graphics/game/menu/menu_bg.png', 		540, 960),

		// BUTTONS
		buttons:[],

		// TOP IMAGES
		imgTitle: new ig.Image('media/graphics/game/menu/menu_title.png', 	433, 236),
		imgTitle_posCur: {x:71, y: 1158}, imgTitle_posEnd: {x:71, y:158},
		imgTitle_rev: {x:71, y:178},
		imgGems: new ig.Image('media/graphics/game/menu/menu_gems.png', 		541, 174),
		imgGems_posCur: {x:0, y: -400}, imgGems_posEnd: {x:0, y:0},

		// BOTTOM IMAGES
		imgPaddle: new ig.Image('media/graphics/game/menu/menu_paddles.png', 	542, 93),
		imgPaddle_posCur: {x:0, y:1067}, imgPaddle_posEnd: {x:0, y:867},
		btnPlay: null, btnUpgrades: null, btnMoreGames: null, btnSettings: null,
		btnPlay_posCur: {x:201, y:1161}, btnPlay_posEnd: {x:201, y:461},
		btnUpgrades_posCur: {x:81, y:1294}, btnUpgrades_posEnd: {x:81, y:594},
		btnSettings_posCur: {x:354, y:1294}, btnSettings_posEnd: {x:354, y:594},
		btnMoreGames_posCur: {x:215, y:1314}, btnMoreGames_posEnd: {x:215, y:624},

		hasPanel: false,

		init:function(x,y,settings){
			if(ig.global.wm)		return;
			this.parent(x,y,settings);

			this.btnPlay 				= ig.game.spawnEntity(EntityBtnMainmenu, this.btnPlay_posCur.x, this.btnPlay_posCur.y);
			this.btnUpgrades 			= ig.game.spawnEntity(EntityBtnUpgrades, this.btnUpgrades_posCur.x, this.btnUpgrades_posCur.y);
			this.btnSettings 			= ig.game.spawnEntity(EntityBtnSettings, this.btnSettings_posCur.x, this.btnSettings_posCur.y);

			if(_SETTINGS.MoreGames.Enabled){
				this.btnMoreGames 		= ig.game.spawnEntity(EntityButtonMoreGames, this.btnMoreGames_posEnd.x, this.btnMoreGames_posEnd.y);
			}else{
				this.tweenBottom_curMax--;
			}

			// Volume & Default Volumes
			var v = ig.game.io.storage.get('speed-pinball-soundVolume');
			if(v == null){
				v = 1;				// Default sound volume
				ig.game.io.storage.set('speed-pinball-soundVolume', v);
			}
			ig.soundHandler.sfxPlayer.volume(v);

			var vm = ig.game.io.storage.get('speed-pinball-musicVolume');
			if(vm == null){
				vm = 0.65;			// Default music volume
				ig.game.io.storage.set('speed-pinball-musicVolume', vm);
			}
			ig.soundHandler.bgmPlayer.volume(vm);

			// Start tweens
			this._tweenTop_Start();
			this._tweenBot_Start();

			// More Games position fix
			if(document.getElementById("more-games")){
				var moreGames=document.getElementById("more-games");
				var canvas=document.getElementById("canvas");
				var gameHeight=960;
				var gameWidth=540;
				var ratioOfPosToHeight=this.btnMoreGames_posEnd.y/gameHeight;
				var ratioOfPosToWidth=this.btnMoreGames_posEnd.x/gameWidth;
				moreGames.style.left = (window.innerWidth-parseInt(moreGames.style.width))/2+"px";
				if(ig.ua.mobile){
					moreGames.style.top =parseInt(canvas.style.height)*ratioOfPosToHeight+"px";
					moreGames.style.left =parseInt(window.innerWidth)*ratioOfPosToWidth+"px";
					moreGames.style.width=106*window.innerWidth/gameWidth+"px";
					moreGames.style.height=116*window.innerHeight/gameHeight+"px";
				}else{
					moreGames.style.top = parseInt(canvas.style.top)+parseInt(canvas.style.height)*ratioOfPosToHeight+"px";
				}
			}
		},

		ready:function(){

		},

		update:function(){
			this.parent();
			ig.game.sortEntitiesDeferred();

			/*Creating gem glow effect*/				this._createGlow();
		},

		draw:function(){
			if(ig.global.wm)		return;

			this.imgBg.draw(0, 0);
			// TOP IMAGES
			this.imgGems.draw(this.imgGems_posCur.x, this.imgGems_posCur.y);
			this.imgTitle.draw(this.imgTitle_posCur.x, this.imgTitle_posCur.y);
			// BOTTOM IMAGES
			this.imgPaddle.draw(this.imgPaddle_posCur.x, this.imgPaddle_posCur.y);
			this.btnPlay.pos			= this.btnPlay_posCur;
			this.btnUpgrades.pos		= this.btnUpgrades_posCur;
			this.btnSettings.pos		= this.btnSettings_posCur;
			if(_SETTINGS.MoreGames.Enabled)
				this.btnMoreGames.pos		= this.btnMoreGames_posCur;
		},

		/*
			PROMPT TUTORIAL
		*/
		_promptTutorial:function(){
			ig.game.spawnEntity(EntityPromptTutorial, 0, -400);
			this.btnPlay.isEnabled = false;
			this.btnUpgrades.isEnabled = false;
			this.btnSettings.isEnabled = false;
			if(_SETTINGS.MoreGames.Enabled)
				this.btnMoreGames.hide();
		},

		/*
		  _____                   _____ _
		 / ____|                 / ____| |
		| |  __  ___ _ __ ___   | |  __| | _____      __
		| | |_ |/ _ \ '_ ` _ \  | | |_ | |/ _ \ \ /\ / /
		| |__| |  __/ | | | | | | |__| | | (_) \ V  V /
		 \_____|\___|_| |_| |_|  \_____|_|\___/ \_/\_/

			MAIN MENU GEM GLOW EFFECT
		*/

		glow: false,
		glowTimer: 0,
		_createGlow: function(){
			if(!this.glow) return;
			this.glowTimer -= ig.system.tick;

			if(this.glowTimer <= 0){
				var nextPosNum = Math.random(), posX = 0, posY = 0;

				if(nextPosNum <= 0.1)			{posX = 330; posY = 75;}
				else if(nextPosNum <= 0.2)		{posX = 164; posY = 30;}
				else if(nextPosNum <= 0.3)		{posX = 147; posY = 111;}
				else if(nextPosNum <= 0.4)		{posX = 48; posY = 75;}
				else if(nextPosNum <= 0.5)		{posX = 276; posY = 35;}
				else if(nextPosNum <= 0.6)		{posX = 122; posY = 23;}
				else if(nextPosNum <= 0.7)		{posX = 367; posY = 43;}
				else if(nextPosNum <= 0.8)		{posX = 373; posY = 94;}
				else if(nextPosNum <= 0.9)		{posX = 477; posY = 51;}
				else if(nextPosNum <= 1)			{posX = 500; posY = 130;}

				ig.game.spawnEntity(EntityGemGlow, posX, posY);
				this.glowTimer = Math.random() * 1.5;
			}
		},

		/*
		_______
	    |__   __|
	       | |_      _____  ___ _ __  ___
	       | \ \ /\ / / _ \/ _ \ '_ \/ __|
	       | |\ V  V /  __/  __/ | | \__ \
	       |_| \_/\_/ \___|\___|_| |_|___/

		  TWEENS BOTTOM
		*/
		tweenBot: null,
		tweenBot_curNum: 0, tweenBot_curMax: 1,
		_tweenBot_Start:function(){
			this.tweenBot_curNum++;

			if(this.tweenBot_curNum > this.tweenBot_curMax){
				this.tweenBot = null;
				return;
			}

			if(this.tweenBot_curNum === 1){
				var duration = 1, endPos = this.imgGems_posEnd ;
				this.tweenBot = this.tween({ imgGems_posCur : endPos}, duration, {
					entity:this, easing: ig.Tween.Easing.Circular.EaseOut,
					onComplete: function() {
						this.entity._tweenBot_Start();
						this.entity.glow = true;
					}
				});
				this.tweenBot.start();
			}
		},

		tweenTop: null,
		tweenTop_curNum: 0, tweenTop_curMax: 6,
		_tweenTop_Start:function(){
			this.tweenTop_curNum++;

			if(this.tweenTop_curNum > this.tweenTop_curMax){
				this.tweenTop = null;
				return;
			}

			if(this.tweenTop_curNum === 1){
				var duration = 0.35, endPos = this.imgTitle_posEnd ;
				this.tweenTop = this.tween({ imgTitle_posCur : endPos}, duration, {
					entity:this, easing: ig.Tween.Easing.Circular.EaseOut,
					onComplete: function() { this.entity._tweenTop_Start(); this.entity._tweenTitle(); }
				});
				this.tweenTop.start();
			}
			else if(this.tweenTop_curNum === 2){
				var duration = 0.15, endPos = this.btnPlay_posEnd ;
				this.tweenTop = this.tween({ btnPlay_posCur : endPos}, duration, {
					entity:this, easing: ig.Tween.Easing.Circular.EaseOut,
					onComplete: function() { this.entity._tweenTop_Start(); }
				});
				this.tweenTop.start();
			}
			else if(this.tweenTop_curNum === 3){
				var duration = 0.15, endPos = this.btnUpgrades_posEnd ;
				this.tweenTop = this.tween({ btnUpgrades_posCur : endPos}, duration, {
					entity:this, easing: ig.Tween.Easing.Circular.EaseOut,
					onComplete: function() { this.entity._tweenTop_Start(); }
				});
				this.tweenTop.start();
			}
			else if(this.tweenTop_curNum === 4){
				var duration = 0.15, endPos = this.btnSettings_posEnd ;
				this.tweenTop = this.tween({ btnSettings_posCur : endPos}, duration, {
					entity:this, easing: ig.Tween.Easing.Circular.EaseOut,
					onComplete: function() { this.entity._tweenTop_Start(); }
				});
				this.tweenTop.start();
			}
			else if(this.tweenTop_curNum === 5){
				if(!_SETTINGS.MoreGames.Enabled){
					this._tweenTop_Start();
					return;
				}

				var duration = 0.15, endPos = this.btnMoreGames_posEnd ;
				this.tweenTop = this.tween({ btnMoreGames_posCur : endPos}, duration, {
					entity:this, easing: ig.Tween.Easing.Circular.EaseOut,
					onComplete: function() { this.entity._tweenTop_Start(); }
				});
				this.tweenTop.start();
			}
			else if(this.tweenTop_curNum === 6){
				var duration = 0.3, endPos = this.imgPaddle_posEnd ;
				this.tweenTop = this.tween({ imgPaddle_posCur : endPos}, duration, {
					entity:this, easing: ig.Tween.Easing.Circular.EaseOut,
					onComplete: function() { this.entity._tweenTop_Start(); }
				});
				this.tweenTop.start();
			}
		},

		tweenTitle: null,
		_tweenTitle:function(){
			var duration = 2, endPos = this.imgTitle_rev ;
			this.tweenTitle = this.tween({ imgTitle_posCur : endPos}, duration, {
				loop:ig.Tween.Loop.Reverse,
			});
			this.tweenTitle.start();
		}
	});
});
