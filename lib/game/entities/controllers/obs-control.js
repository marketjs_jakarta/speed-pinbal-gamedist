ig.module('game.entities.controllers.obs-control')
.requires(
	'impact.entity',
	'game.entities.controllers.game-control-ui'
)
.defines(function() {
	EntityObsControl = ig.Entity.extend({
          name: 'obsControl',

          obsToCreate:null,
          obsCount: 0,
          obsToSpawn:null,

          gem_ReqToSpawn: 7,
          gem_Last: 0,

          init:function(x,y,settings){
			if (ig.global.wm) return;

               this._createMap();

               this.parent(x,y,settings);
          },

          _createMap:function(){
			var  mapCount = 4,
				randMap = Math.floor(Math.random() * mapCount - 1) + 1;
			if(randMap > mapCount || randMap < 0)	randMap = mapCount;

			switch(randMap){
				/*case 0: case 1: case 2: case 3:
					// Fill board with obstacles for testing:
					this.obsToCreate = [
						{type: 1, posX: 400, posY: 496},
						{type: 1, posX: 307, posY: 496},
						{type: 1, posX: 221, posY: 494},
						{type: 1, posX: 221, posY: 494},
						{type: 1, posX: 138, posY: 496},
						{type: 1, posX: 457, posY: 494},
						{type: 1, posX: 50, posY: 496},
						{type: 1, posX: 434, posY: 598},
						{type: 1, posX: 341, posY: 598},
						{type: 1, posX: 255, posY: 598},
						{type: 1, posX: 172, posY: 598},
						{type: 1, posX: 84, posY: 598},
						{type: 1, posX: 434, posY: 405},
						{type: 1, posX: 341, posY: 405},
						{type: 1, posX: 255, posY: 403},
						{type: 1, posX: 172, posY: 405},
						{type: 1, posX: 84, posY: 405},
						{type: 1, posX: 403, posY: 336},
						{type: 1, posX: 300, posY: 313},
						{type: 1, posX: 218, posY: 313},
						{type: 1, posX: 138, posY: 313},
						{type: 1, posX: 339, posY: 217},
						{type: 1, posX: 260, posY: 217},
						{type: 1, posX: 180, posY: 217},
					];
				break;*/
				case 0:
		               this.obsToCreate = [
		                    //{type: 1, posX: 251, posY: 296},{type: 2, posX: 146, posY: 430},{type: 2, posX: 356, posY: 430},{type: 1, posX: 251, posY: 475},{type: 4, posX: 397, posY: 659},{type: 3, posX: 360, posY: 530},{type: 3, posX: 101, posY: 530},{type: 9, posX: 108, posY: 659},
						{type: 4, posX: 387, posY: 567},
						{type: 1, posX: 330, posY: 334},
						{type: 1, posX: 178, posY: 334},
						{type: 1, posX: 88, posY: 440},
						{type: 1, posX: 419, posY: 440},
						{type: 3, posX: 226, posY: 217},
						{type: 2, posX: 330, posY: 440},
						{type: 2, posX: 178, posY: 440},
						{type: 9, posX: 89, posY: 568},
					];
				break;
				case 1:
		               this.obsToCreate = [
		                    //{type: 8, posX: 204, posY: 506},{type: 4, posX: 397, posY: 659},{type: 9, posX: 75, posY: 660},{type: 5, posX: 403, posY: 385},{type: 10, posX: 43, posY: 379},{type: 7, posX: 245, posY: 411},{type: 6, posX: 344, posY: 331},{type: 6, posX: 153, posY: 331},
						{type: 9, posX: 88, posY: 652},
						{type: 4, posX: 387, posY: 653},
						{type: 3, posX: 226, posY: 378},
						{type: 2, posX: 419, posY: 347},
						{type: 2, posX: 353, posY: 278},
						{type: 2, posX: 155, posY: 278},
						{type: 2, posX: 89, posY: 356},
						{type: 3, posX: 330, posY: 481},
						{type: 6, posX: 243, posY: 257},
						{type: 3, posX: 136, posY: 481},
					];
				break;
				case 2:
					this.obsToCreate = [
						{type: 7, posX: 244, posY: 428},
						{type: 6, posX: 327, posY: 287},
						{type: 6, posX: 165, posY: 287},
						{type: 9, posX: 89, posY: 653},
						{type: 4, posX: 387, posY: 653},
						{type: 5, posX: 402, posY: 447},
						{type: 10, posX: 43, posY: 447},
					];
				break;
				case 3:
					this.obsToCreate = [
						{type: 7, posX: 362, posY: 599},
						{type: 6, posX: 243, posY: 393},
						{type: 7, posX: 136, posY: 599},
						{type: 9, posX: 89, posY: 428},
						{type: 4, posX: 387, posY: 428},
						{type: 2, posX: 250, posY: 224},
						{type: 2, posX: 327, posY: 290},
						{type: 2, posX: 176, posY: 290},
					];
				break;
			}

               this.obsCount = this.obsToCreate.length;
          },

          _spawnObs:function(){
               if(this.obsCount <= 0)   return;

               if(ig.game.curGems % this.gem_ReqToSpawn == 0 && ig.game.curGems > this.gem_Last){
                    var newObsNum  = Math.floor(Math.random() * (this.obsToCreate.length - 1));
                    this.obsToSpawn     = this.obsToCreate[newObsNum];

                    this.obsToCreate.splice(newObsNum, 1);
                    this.obsCount = this.obsToCreate.length;
               }
          },

          update:function(){
               if(this.obsToSpawn == null)   return;

               var newObs = this.obsToSpawn;
               switch(newObs.type){
                    case 1: ig.game.spawnEntity(EntityObstacle1, newObs.posX, newObs.posY); break;
                    case 2: ig.game.spawnEntity(EntityObstacle2, newObs.posX, newObs.posY); break;
                    case 3: ig.game.spawnEntity(EntityObstacle3, newObs.posX, newObs.posY); break;
                    case 4: ig.game.spawnEntity(EntityObstacle4, newObs.posX, newObs.posY); break;
                    case 5: ig.game.spawnEntity(EntityObstacle5, newObs.posX, newObs.posY); break;
                    case 6: ig.game.spawnEntity(EntityObstacle6, newObs.posX, newObs.posY); break;
                    case 7: ig.game.spawnEntity(EntityObstacle7, newObs.posX, newObs.posY); break;
                    case 8: ig.game.spawnEntity(EntityObstacle8, newObs.posX, newObs.posY); break;
                    case 9: ig.game.spawnEntity(EntityObstacle9, newObs.posX, newObs.posY); break;
				case 10: ig.game.spawnEntity(EntityObstacle10, newObs.posX, newObs.posY); break;
               }
               this.obsToSpawn = null;
          }
     });
});
