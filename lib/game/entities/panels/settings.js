ig.module('game.entities.panels.settings')
.requires(
	'impact.entity',
	'game.entities.buttons.btn'
)
.defines(function() {
	EntitySettings = ig.Entity.extend({

		name: 'settingsControl',
		zIndex: 801,
		pointer: null,

		// Images
		imgBar: new ig.Image('media/graphics/game/settings/settings-bar.png'),
		sizeBar: {x:340, y:22},
		imgBase: new ig.Image('media/graphics/game/settings/settings-barbase.png'),
		imgMusic: new ig.Image('media/graphics/game/settings/settings-music.png'),
		imgSound: new ig.Image('media/graphics/game/settings/settings-sound.png'),
		imgKnob: new ig.Image('media/graphics/game/settings/settings-knob.png'),

		// Positions
		musicSliderKnob: {x:124, y:350, w:349, h:39},
		soundSliderKnob: {x:124, y:424, w:349, h:39},

		// Misc
		btnOk: null, btnResume: null, btnHome: null, btnRestart: null,
		isPause: false,

		lockToSoundRect: false,
		lockToMusicRect: false,

		init:function(x,y,settings){
			this.parent(x,y,settings);
			//ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.transition);

			// Objects
			this.pointer = ig.game.getEntitiesByType(EntityPointer)[0];
			if(!settings.isPause){
				this.btnOk 			= ig.game.spawnEntity(EntityBtnSettingsOk, 766, 615, {settings:this, posOffset: {x:216, y:615}});
			}else{
				this.btnResume 		= ig.game.spawnEntity(EntityBtnResume, 766, 615, {settings:this, posOffset: {x:200, y:537}});
				this.btnHome 			= ig.game.spawnEntity(EntityBtnGameover, 624, 615, {settings:this, posOffset: {x:74, y:615}, isPause: true});
				this.btnRestart 		= ig.game.spawnEntity(EntityBtnRestart, 906, 615, {settings:this, posOffset: {x:356, y:615}, isPause: true});
			}

			this._open();
		},

		update:function(){
			this._checkClick();
			this.parent();
		},

		draw:function(){
			var ctx = ig.system.context;
			ctx.save();

			// Rectangle background
			ctx.beginPath();
			ctx.rect(this.pos.x, this.pos.y, ig.system.width, ig.system.height);
			ctx.fillStyle = '#411e4b';
			ctx.fill();

			// Settings text
			ctx.font = '70px Somatic Rounded, Helvetica, Verdana';
			ctx.fillStyle = '#aecdf9';
			ctx.textAlign = "center";
			if(!this.isPause)
				ctx.fillText(_STRINGS['Titles']['Settings'], this.pos.x + 270, this.pos.y + 200);
			else
				ctx.fillText(_STRINGS['Titles']['Pause'], this.pos.x + 270, this.pos.y + 200);

			// Symbols
			this.imgMusic.draw(this.pos.x + 73, this.pos.y + 349);
			this.imgSound.draw(this.pos.x + 70, this.pos.y + 425);

			// Bar base
			this.imgBase.draw(this.pos.x + 124, this.pos.y + 354);
			this.imgBase.draw(this.pos.x + 124, this.pos.y + 428);

			// Music
			var w = this.imgKnob.width,
				p = ig.soundHandler.bgmPlayer.getVolume(),
				v = (this.musicSliderKnob.w - w)*p;
			if(isNaN(p)) 	p = 1;

			// Music Bar
			if(p > 0){
				var widthBar = this.sizeBar.x*p;
				this.imgBar.draw(this.pos.x + 129, this.pos.y + 357, 0, 0, widthBar, this.sizeBar.y);
			}

			// Music Knob
			this.imgKnob.draw(
				this.musicSliderKnob.x + v + this.pos.x,
				this.musicSliderKnob.y + this.pos.y
			);

			// Sound
			p = ig.soundHandler.sfxPlayer.getVolume();
			v = (this.soundSliderKnob.w - w)*p;
			if(isNaN(p)) 	p = 1;

			// Sound Bar
			if(p > 0){
				var widthBar = this.sizeBar.x*p;
				this.imgBar.draw(this.pos.x + 129, this.pos.y + 431, 0, 0, widthBar, this.sizeBar.y);
			}

			// Sound Knob
			this.imgKnob.draw(
				this.soundSliderKnob.x + v + this.pos.x,
				this.soundSliderKnob.y + this.pos.y
			);

			ctx.restore();
		},

		/*
			_______
		    |__   __|
		       | |_      _____  ___ _ __  ___
		       | \ \ /\ / / _ \/ _ \ '_ \/ __|
		       | |\ V  V /  __/  __/ | | \__ \
		       |_| \_/\_/ \___|\___|_| |_|___/

			  TWEENS
		*/
		_open:function(){
			var duration = 0.55, targetPos = {x:0, y:0};
			this.tweenSet = this.tween({pos : targetPos}, duration, {
				entity:this,
				easing:ig.Tween.Easing.Quadratic.EaseInOut,
				onComplete: function(){
					this.entity.tweenSet = null;
					this.entity._openEnd();
					this.entity.isEnabled = true;
				}
			});
			this.tweenSet.start();
		},

		_openEnd:function(){
			if(!this.isPause){
				this.btnOk.isEnabled 			= true;
			}else{
				 this.btnResume.isEnabled 		= true;
				 this.btnHome.isEnabled 			= true;
				 this.btnRestart.isEnabled 		= true;
			}
		},

		_closeStart:function(){
			// Disable buttons
			if(!this.isPause){
				this.btnOk.isEnabled 			= false;
			}else{
				this.btnResume.isEnabled 		= false;
				this.btnHome.isEnabled 			= false;
				this.btnRestart.isEnabled 		= false;
			}

			var duration = 0.55, targetPos = {x:550, y:0};
			this.tweenUpg = this.tween({pos : targetPos}, duration, {
				entity:this,
				easing:ig.Tween.Easing.Quadratic.EaseInOut,
				onComplete: function(){
					this.entity.tweenUpg = null;
					this.entity._close();
				}
			});
			this.tweenUpg.start();

		},

		_close: function(){
			if(_SETTINGS.MoreGames.Enabled){
				var moregames = ig.game.getEntitiesByType(EntityButtonMoreGames)[0];
				if(moregames != null) moregames.show();
			}

			if(!this.isPause){
				this.btnOk.kill();
			}else{
				// Resume game
				var gameControl = ig.game.getEntitiesByType(EntityGameControl)[0];
				gameControl._gamePause(false);

				this.btnResume.kill();
				this.btnHome.kill();
				this.btnRestart.kill();
			}

			// Enable main menu buttons
			var 	btnPlay = ig.game.getEntitiesByType(EntityBtnMainmenu)[0],
				btnUpgrades = ig.game.getEntitiesByType(EntityBtnUpgrades)[0],
				btnSettings = ig.game.getEntitiesByType(EntityBtnSettings)[0];
			if(btnPlay)			btnPlay.isEnabled = true;
			if(btnUpgrades)		btnUpgrades.isEnabled = true;
			if(btnSettings)		btnSettings.isEnabled = true;

			this.kill();
		},
		/*
			_____                   _
		    |_   _|                 | |
		      | |  _ __  _ __  _   _| |_ ___
		      | | | '_ \| '_ \| | | | __/ __|
		     _| |_| | | | |_) | |_| | |_\__ \
		    |_____|_| |_| .__/ \__,_|\__|___/
		                | |
		                |_|

			INPUTS
		*/
		isEnabled: false,
		_checkClick: function(){
			if(this.pointer == null || !this.isEnabled)	return;
			this.pointer.refreshPos();

			if( !this.lockToSoundRect && !this.lockToMusicRect &&
				this.pointer.isFirstPressed && !this.pointer.isReleased ){
					var pointer = {};
					pointer.x = this.pointer.pos.x;
					pointer.y = this.pointer.pos.y;

					if(this._rectangleClickCheck(pointer, this.soundSliderKnob)){
						this.lockToSoundRect = true;
					}else if(this._rectangleClickCheck(pointer, this.musicSliderKnob)){
						this.lockToMusicRect = true;
					}
			}
			else if(this.pointer.isReleased){
				if(this.lockToSoundRect || this.lockToMusicRect)
					ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.click);

				this.lockToSoundRect = false;
				this.lockToMusicRect = false;

				// For click sound
				/*
				var pointer = {};
				pointer.x = this.pointer.pos.x;
				pointer.y = this.pointer.pos.y;

				if( this._rectangleClickCheck(pointer, this.soundSliderKnob) ||
					this._rectangleClickCheck(pointer, this.musicSliderKnob))
					// Click sound here
				*/
			}

			if(this.lockToSoundRect){
				var w = this.imgKnob.width/2;
				var v = (this.pointer.pos.x + this.pointer.size.x/2) - this.soundSliderKnob.x;
				if( v < w ) v = w;
				if( v > this.soundSliderKnob.w - w) v = this.soundSliderKnob.w - w;
				var v = (v-w)/(this.soundSliderKnob.w-w-w);

				ig.soundHandler.sfxPlayer.volume(v);
                	ig.game.io.storage.set('speed-pinball-soundVolume', v);
			}
			else if(this.lockToMusicRect){
				var w = this.imgKnob.width/2;
				var v = (this.pointer.pos.x + this.pointer.size.x/2) - this.musicSliderKnob.x;
				if( v < w ) v = w;
				if( v > this.musicSliderKnob.w - w) v = this.musicSliderKnob.w - w;
				var v = (v-w)/(this.musicSliderKnob.w-w-w);

				ig.soundHandler.bgmPlayer.volume(v);
                	ig.game.io.storage.set('speed-pinball-musicVolume', v);
			}
		},

		_rectangleClickCheck: function(pointer, rectangle){
		return		(	pointer.x > rectangle.x &&
						pointer.x < rectangle.x + rectangle.w &&
						pointer.y > rectangle.y &&
						pointer.y < rectangle.y + rectangle.h	);
		}
	});

	/*
		____        _   _
		|  _ \      | | | |
		| |_) |_   _| |_| |_ ___  _ __  ___
		|  _ <| | | | __| __/ _ \| '_ \/ __|
		| |_) | |_| | |_| || (_) | | | \__ \
		|____/ \__,_|\__|\__\___/|_| |_|___/

		BUTTONS
	*/

	// OK BUTTON
	EntityBtnSettingsOk = EntityBtn.extend({
          logoImg: new ig.Image('media/graphics/game/btn_ok.png', 106, 116),
          size:{x:106, y:116},
          name: 'btnSettingsOk',
		zIndex: 900,
		posOffset: {x:0, y:0},

		settings: null,
		isEnabled: false,

		update:function(){
			this.parent();
			this.pos.x = this.settings.pos.x + this.posOffset.x;
			this.pos.y = this.settings.pos.y + this.posOffset.y;
		},

          interact:function(){
			this.settings._closeStart();

			var mainMenuControl = ig.game.getEntitiesByType(EntityMenuControl)[0];
			if(mainMenuControl)
				mainMenuControl.hasPanel = false;
          }
     });

	// RESUME BUTTON
	EntityBtnResume = EntityBtn.extend({
          logoImg: new ig.Image('media/graphics/game/menu/btn_play.png', 140, 151),
          size:{x:140, y:151},
          name: 'btnResume',
		zIndex: 900,
		posOffset: {x:0, y:0},

		settings: null,
		isEnabled: false,

		update:function(){
			this.parent();
			this.pos.x = this.settings.pos.x + this.posOffset.x;
			this.pos.y = this.settings.pos.y + this.posOffset.y;
		},

          interact:function(){
			this.settings._closeStart();

			var pauseBtn = ig.game.getEntitiesByType(EntityBtnGame)[0];
			if(pauseBtn){
				pauseBtn.isEnabled = true;
			}
          }
     });
});
