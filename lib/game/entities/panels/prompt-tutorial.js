ig.module('game.entities.panels.prompt-tutorial')
.requires(
	'impact.entity',
	'game.entities.buttons.btn'
)
.defines(function() {
	EntityPromptTutorial = ig.Entity.extend({
		zIndex: 2000,

		btnNo: null, btnYes: null,

          init: function(x,y,settings){
			this.parent(x,y,settings);
			this.btnNo = ig.game.spawnEntity(EntityBtnTutorialNo, 100, this.pos.y, {tutorial:this});
			this.btnYes = ig.game.spawnEntity(EntityBtnTutorialYes, 340, this.pos.y, {tutorial:this});
			this.btnNo.isEnabled = false; this.btnYes.isEnabled = false;

			this._open();		// Tween
          },

		update: function(){
			this.parent();
		},

          draw: function(){
			var ctx = ig.system.context;
               ctx.save();

			// Black Rectangle background
			ctx.beginPath();
			ctx.rect(0, 0, ig.system.width, ig.system.height);
			ctx.globalAlpha = 0.5;
			ctx.fillStyle = '#000000';
			ctx.fill();

               // Rectangle background
			ctx.beginPath();
			ctx.rect(this.pos.x, this.pos.y - 100, ig.system.width, 240);
			ctx.globalAlpha = 1;
			ctx.fillStyle = '#3c1d4a';		// #882222
			ctx.fill();

			// Text
			ctx.font = '50px Somatic Rounded, Helvetica, Verdana';
			ctx.globalAlpha = 1;
			ctx.fillStyle = '#aed6f3';
			ctx.textAlign = "center";
			ctx.fillText(_STRINGS["Titles"]["Tutorial?"], 270, this.pos.y - 25);

			ctx.restore();
          },

		/*
			_______
		    |__   __|
			  | |_      _____  ___ _ __  ___
			  | \ \ /\ / / _ \/ _ \ '_ \/ __|
			  | |\ V  V /  __/  __/ | | \__ \
			  |_| \_/\_/ \___|\___|_| |_|___/

			  TWEENS
		*/

		_open:function(){
			var duration = 0.55, targetPos = {x:0, y:500};
			this.tweenGO = this.tween({pos : targetPos}, duration, {
				entity:this,
				easing:ig.Tween.Easing.Quadratic.EaseInOut,
				onComplete: function(){
					this.entity.tweenGO = null;
					this.entity._openEnd();
				}
			});
			this.tweenGO.start();
		},

		_openEnd:function(){
			this.btnNo.isEnabled = true;
			this.btnYes.isEnabled = true;
		}
     });
});
