ig.module('game.entities.panels.upgrades')
.requires(
	'impact.entity',
	'game.entities.buttons.btn'
)
.defines(function() {
	EntityUpgrades = ig.Entity.extend({
          name: 'upgradesControl',
     	zIndex: 1001,
     	pointer: null,

		imgGem: new ig.Image('media/graphics/game/ui/gemui.png'),
          upgrades: [],
		btnClose: null,

          init:function(x,y,settings){
			this.parent(x,y,settings);
			//ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.transition);

               this.pointer = ig.game.getEntitiesByType(EntityPointer)[0];
			this.btnClose = ig.game.spawnEntity(EntityBtnUpgradeClose, x, y, {upgWindow: this});

               // Upgrades
               /*Ball Count*/                     this.upgrades.push(ig.game.spawnEntity(EntityUpgradeBar, 45, 227, {upgControl: this, name:'u_ballCount', upgNum: 1}));
               /*Countdown*/                      this.upgrades.push(ig.game.spawnEntity(EntityUpgradeBar, 45, 365, {upgControl: this, name:'u_countdown', upgNum: 2}));
               /*Sure Shot*/                      this.upgrades.push(ig.game.spawnEntity(EntityUpgradeBar, 45, 503, {upgControl: this, name:'u_sureShot', upgNum: 3}));
               /*Magnet*/                         this.upgrades.push(ig.game.spawnEntity(EntityUpgradeBar, 45, 641, {upgControl: this, name:'u_magnet', upgNum: 4}));
               /*Spring*/                         this.upgrades.push(ig.game.spawnEntity(EntityUpgradeBar, 45, 779, {upgControl: this, name:'u_spring', upgNum: 5}));

			this._open();
          },

		update:function(){
			this.parent();
		},

          draw:function(){
               var ctx = ig.system.context;
               ctx.save();

               // Rectangle background
               ctx.beginPath();
               ctx.rect(this.pos.x, this.pos.y, ig.system.width, ig.system.height);
               ctx.fillStyle = '#411e4b';
               ctx.fill();

               // Upgrade title text
               ctx.font = '70px Somatic Rounded, Helvetica, Verdana';
               ctx.fillStyle = '#aecdf9';
               ctx.textAlign = "center";
               ctx.fillText(_STRINGS['Titles']['Upgrades'], this.pos.x + 270, this.pos.y + 140);

			// Player's gems
			this.imgGem.draw(this.pos.x + 215, 165);
			ctx.font = '35px Somatic Rounded, Helvetica, Verdana';
               ctx.fillStyle = '#aecdf9';
               ctx.textAlign = "left";
               ctx.fillText(ig.game.playerGems, this.pos.x + 277, this.pos.y + 195);

               ctx.restore();
          },

		_open:function(){
			var duration = 0.55, targetPos = {x:0, y:0};
			this.tweenUpg = this.tween({pos : targetPos}, duration, {
				entity:this,
				easing:ig.Tween.Easing.Quadratic.EaseInOut,
				onComplete: function(){
					this.entity.tweenUpg = null;
					this.entity._openEnd();
				}
			});
			this.tweenUpg.start();
		},

		_openEnd:function(){
			// Enable buttons
			for(var i = this.upgrades.length-1; i >= 0; i--){
				this.upgrades[i].btnUpgrade.isEnabled = true;
			}
			this.btnClose.isEnabled = true;
		},

		_closeStart:function(){
			// Disable buttons
			for(var i = this.upgrades.length-1; i >= 0; i--){
				this.upgrades[i].btnUpgrade.isEnabled = false;
			}
			this.btnClose.isEnabled = false;

			var duration = 0.55, targetPos = {x:550, y:0};
			this.tweenUpg = this.tween({pos : targetPos}, duration, {
				entity:this,
				easing:ig.Tween.Easing.Quadratic.EaseInOut,
				onComplete: function(){
					this.entity.tweenUpg = null;
					this.entity._close();
				}
			});
			this.tweenUpg.start();
		},

          _close:function(){
			if(_SETTINGS.MoreGames.Enabled){
				var moregames = ig.game.getEntitiesByType(EntityButtonMoreGames)[0];
				if(moregames != null) moregames.show();
			}

               for(var i = this.upgrades.length-1; i >= 0; i--){
                    this.upgrades[i]._close();
               }

			// Enable main menu buttons
			var 	btnPlay = ig.game.getEntitiesByType(EntityBtnMainmenu)[0],
				btnUpgrades = ig.game.getEntitiesByType(EntityBtnUpgrades)[0],
				btnSettings = ig.game.getEntitiesByType(EntityBtnSettings)[0];
			if(btnPlay)			btnPlay.isEnabled = true;
			if(btnUpgrades)		btnUpgrades.isEnabled = true;
			if(btnSettings)		btnSettings.isEnabled = true;
			
			this.btnClose.kill();
               this.kill();
          }
     });

     /*
           _    _                           _        ____
          | |  | |                         | |      |  _ \
          | |  | |_ __   __ _ _ __ __ _  __| | ___  | |_) | __ _ _ __
          | |  | | '_ \ / _` | '__/ _` |/ _` |/ _ \ |  _ < / _` | '__|
          | |__| | |_) | (_| | | | (_| | (_| |  __/ | |_) | (_| | |
           \____/| .__/ \__, |_|  \__,_|\__,_|\___| |____/ \__,_|_|
                 | |     __/ |
                 |_|    |___/
          UPGRADE BAR
     */
     EntityUpgradeBar = ig.Entity.extend({
          name: null,
          zIndex: 1010,

          // Images
          imgBase:            new ig.Image('media/graphics/game/ui/upg-bar.png'),
		imgUpgSlot:		new ig.Image('media/graphics/game/ui/upg-slot.png'),
          imgSym_Ball:        new ig.Image('media/graphics/game/ui/upg-ball.png'),
          imgSym_Time:        new ig.Image('media/graphics/game/ui/upg-time.png'),
          imgSym_SureShot:    new ig.Image('media/graphics/game/ui/upg-sureshot.png'),
          imgSym_Magnet:      new ig.Image('media/graphics/game/ui/upg-magnet.png'),
          imgSym_Spring:      new ig.Image('media/graphics/game/ui/upg-spring.png'),

          // Misc
		upgControl: null,
          btnUpgrade: null,
		upgValue: 0, upgValue_Max: 4,
		upgNum: 0,
		cost: {
			1: [10, 20, 40, 80],
			2: [10, 20, 40, 80],
			3: [10, 20, 40, 80],
			4: [10, 20, 40, 80],
			5: [10, 20, 40, 80],
		},
		upgDesc: {
			1: ['1→2', '2→3', '3→4', '4→5', '5'],
			2: ['16s→18s', '18s→20s', '20s→22s', '22s→24s', '24s'],
			3: ['1m→50s', '50s→40s', '40s→30s', '30s→20s', '20s'],
			4: ['5s→7s', '7s→9s', '9s→11s', '11s→13s', '13s'],
			5: ['0→1', '1→2', '2→3', '3→4', '4'],
		},

		drawPos: {x:0, y:0},

          init:function(x,y,settings){
			this.drawPos = {x:x, y:y};

			var btnPos = {x: x+371, y: y+45};

			this.btnUpgrade = ig.game.spawnEntity(EntityUpgradeButton, this.drawPos.x + x + 371, this.drawPos.y + y + 45, {parentBar: this, posOffset: {x:btnPos.x, y:btnPos.y}});
			this.upgValue = ig.game.io.storage.get('speed-pinball-' + settings.name) || 0;

               this.parent(x,y,settings);
          },

		update:function(){
			this.parent();
			this.drawPos.x = this.upgControl.pos.x;
			this.drawPos.y = this.upgControl.pos.y;
		},

          draw:function(){
               var ctx = ig.system.context;
               ctx.save();

			var drawX = this.drawPos.x + this.pos.x, drawY = this.drawPos.y + this.pos.y;

               this.imgBase.draw(drawX, drawY);

               // Upgrade icon
               switch(this.name){
                    case 'u_ballCount':                    this.imgSym = this.imgSym_Ball.draw(this.drawPos.x + this.pos.x + 13, this.drawPos.y + this.pos.y + 21); break;
                    case 'u_countdown':                    this.imgSym = this.imgSym_Time.draw(this.drawPos.x + this.pos.x + 13, this.drawPos.y + this.pos.y + 21); break;
                    case 'u_sureShot':                     this.imgSym = this.imgSym_SureShot.draw(this.drawPos.x + this.pos.x + 13, this.drawPos.y + this.pos.y + 21); break;
                    case 'u_magnet':                       this.imgSym = this.imgSym_Magnet.draw(this.drawPos.x + this.pos.x + 13, this.drawPos.y + this.pos.y + 21); break;
                    case 'u_spring':                       this.imgSym = this.imgSym_Spring.draw(this.drawPos.x + this.pos.x + 13, this.drawPos.y + this.pos.y + 21); break;
               }

			// Upgrade Title Text
			ctx.font = '23px Somatic Rounded, Helvetica, Verdana';
               ctx.fillStyle = '#aecdf9';
               ctx.textAlign = "left";
               ctx.fillText(_STRINGS['Upgrades'][this.name], this.drawPos.x + this.pos.x + 97, this.drawPos.y + this.pos.y + 32);

			if(this.upgValue < this.upgValue_Max){
				// Upgrade Cost Text
				ctx.font = '20px Somatic Rounded, Helvetica, Verdana';
	               ctx.fillStyle = '#aecdf9';
	               ctx.textAlign = "left";
	               ctx.fillText(this.cost[this.upgNum][this.upgValue], this.drawPos.x + this.pos.x + 400, this.drawPos.y + this.pos.y + 30);
			}

			// Upgrade Effect Text
			ctx.font = '22px Somatic Rounded, Helvetica, Verdana';
			ctx.fillStyle = '#aecdf9';
			ctx.textAlign = "left";
			ctx.fillText(this.upgDesc[this.upgNum][this.upgValue], this.drawPos.x + this.pos.x + 97, this.drawPos.y + this.pos.y + 93);

			// Upgrade slots
			for(var i = 1; i <= this.upgValue; i++){
				this.imgUpgSlot.draw(this.drawPos.x + 87 + 58 * i, this.drawPos.y + this.pos.y + 48);
			}
			ig.game.upgradeSet();

               ctx.restore();
          },

		_upgrade:function(){
			var upgCost = this.cost[this.upgNum][this.upgValue];
			if(upgCost > ig.game.playerGems)			{console.log('not enough gems');return;}
			if(this.upgValue >= this.upgValue_Max)		{console.log('already at max level');return;}

			ig.game.playerGems -= upgCost;
			ig.game.io.storage.set('speed-pinball-gems', ig.game.playerGems);

			this.upgValue++;
			if(this.upgValue_Max <= this.upgValue)			this.upgValue = this.upgValue_Max;
			ig.game.io.storage.set('speed-pinball-' + this.name, this.upgValue);

			ig.soundHandler.sfxPlayer.play(ig.soundHandler.sfxPlayer.soundList.upgrade);
		},

          _close:function(){
               this.btnUpgrade.kill();
               this.kill();
          }
     });

     /*
           ____        _   _
          |  _ \      | | | |
          | |_) |_   _| |_| |_ ___  _ __  ___
          |  _ <| | | | __| __/ _ \| '_ \/ __|
          | |_) | |_| | |_| || (_) | | | \__ \
          |____/ \__,_|\__|\__\___/|_| |_|___/

          BUTTONS
     */
     EntityUpgradeButton = EntityBtn.extend({
          logoImg: new ig.Image('media/graphics/game/ui/btn_upgsmall.png', 54, 60),
          size:{x:54, y:60},
          name: 'btnSettingsOk',
		zIndex: 1020,
		posOffset: {x:0, y:0}, show: false,

		parentBar: null,
		isEnabled: false,

		update:function(){
			this.parent();

			if(!this.show){
				this.pos.x = 5000;
				this.show = true;
			}else{
				this.pos.x = this.parentBar.drawPos.x + this.posOffset.x;
				this.pos.y = this.parentBar.drawPos.y + this.posOffset.y;
			}
		},

          interact:function(){
			this.parentBar._upgrade();
          }
     });

	// CLOSE BUTTON
	EntityBtnUpgradeClose = EntityBtn.extend({
          logoImg: new ig.Image('media/graphics/game/ui/btn_close.png', 51, 56),
          size:{x:51, y:56},
          name: 'btnUpgClose',
		zIndex: 1010,
		posOffset: {x:464, y:14},

		upgWindow: null,
		isEnabled: false,

		update:function(){
			this.parent();
			this.pos.x = this.upgWindow.pos.x + this.posOffset.x;
			this.pos.y = this.upgWindow.pos.y + this.posOffset.y;
		},

          interact:function(){
			this.upgWindow._closeStart();

			var mainMenuControl = ig.game.getEntitiesByType(EntityMenuControl)[0];
			if(mainMenuControl)
				mainMenuControl.hasPanel = false;
          }
     });
});
