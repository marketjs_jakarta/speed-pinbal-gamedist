ig.module('game.entities.panels.gameover')
.requires(
	'impact.entity',
	'game.entities.buttons.btn'
)
.defines(function() {
	EntityGameover = ig.Entity.extend({
          name: 'gameOverControl',

          imgGem: new ig.Image('media/graphics/game/ui/gemui.png'),
          imgBg: new ig.Image('media/graphics/game/menu/menu_bg.png'),
          zIndex: 2000,

          btnHome: null, btnRestart: null,
		restartHotkey: 'Space', canRestart: false,

          init:function(x,y,settings){
			this.parent(x,y,settings);

               // BUTTONS
               this.btnHome        = ig.game.spawnEntity(EntityBtnGameover, x+125, y+620, {posOffset:{x:125, y:620}});
               this.btnRestart     = ig.game.spawnEntity(EntityBtnRestart, x+311, y+620, {posOffset:{x:311, y:620}});

               this._open();

               //////////////////////// API_END_GAME ////////////////////////
               if(ig.game.curGems > ig.game.hiScore){
                    // Get hi-score
                    ig.game.hiScore = ig.game.curGems;
                    ig.game.io.storage.set('speed-pinball-hiScore', ig.game.curGems);
               }
               //////////////////////// API_END_GAME ////////////////////////
          },

          update:function(){
               this.parent();

               // Button positions
               this.btnHome.pos.x = this.pos.x + this.btnHome.posOffset.x;
               this.btnHome.pos.y = this.pos.y + this.btnHome.posOffset.y;
               this.btnRestart.pos.x = this.pos.x + this.btnRestart.posOffset.x;
               this.btnRestart.pos.y = this.pos.y + this.btnRestart.posOffset.y;

			// Restart hotkey
			if(ig.input.state(this.restartHotkey) && this.canRestart){
				ig.game.director.jumpTo(LevelTestGame);
			}
          },

          draw:function(){
               var ctx = ig.system.context;
               ctx.save();

               // Rectangle background
			/*ctx.beginPath();
			ctx.rect(this.pos.x, this.pos.y, ig.system.width, ig.system.height);
			ctx.fillStyle = '#411e4b';
			ctx.fill();*/
               // Patterned background
               this.imgBg.draw(this.pos.x, this.pos.y);

               // Gameover Title Text
			ctx.font = '70px Somatic Rounded, Helvetica, Verdana';
               ctx.fillStyle = '#aecdf9';
               ctx.textAlign = "center";
               ctx.fillText(_STRINGS['Titles']['GameOver'], this.pos.x + 270, this.pos.y + 200);

               // Score Text
			ctx.font = '51px Nord, Helvetica, Verdana';
               ctx.fillStyle = '#ffffff';
               ctx.textAlign = "center";
               ctx.fillText(_STRINGS['Game']['Score'] + '  ' + ig.game.curGems, this.pos.x + 270, this.pos.y + 328);

               // Best Text
			ctx.font = '51px Nord, Helvetica, Verdana';
               ctx.fillStyle = '#ef5691';
               ctx.textAlign = "center";
               ctx.fillText(_STRINGS['Game']['Best'] + '  ' + ig.game.hiScore, this.pos.x + 270, this.pos.y + 419);

               // Current gems text
               this.imgGem.draw(this.pos.x + 214, this.pos.y + 485);
               ctx.font = '35px Nord, Helvetica, Verdana';
               ctx.fillStyle = '#ffffff';
               ctx.textAlign = "left";
               ctx.fillText(ig.game.playerGems, this.pos.x + 285, this.pos.y + 515);

               ctx.restore();
          },

          /*
			_______
		    |__   __|
		       | |_      _____  ___ _ __  ___
		       | \ \ /\ / / _ \/ _ \ '_ \/ __|
		       | |\ V  V /  __/  __/ | | \__ \
		       |_| \_/\_/ \___|\___|_| |_|___/

			  TWEENS
		*/

          _open:function(){
			var duration = 0.55, targetPos = {x:0, y:0};
			this.tweenGO = this.tween({pos : targetPos}, duration, {
				entity:this,
				easing:ig.Tween.Easing.Quadratic.EaseInOut,
				onComplete: function(){
					this.entity.tweenGO = null;
					this.entity._openEnd();
				}
			});
			this.tweenGO.start();
		},

          _openEnd:function(){
               this.btnHome.isEnabled = true;
               this.btnRestart.isEnabled = true;
			this.canRestart = true;
          }
     });
});
