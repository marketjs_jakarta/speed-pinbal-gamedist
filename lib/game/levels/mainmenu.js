ig.module( 'game.levels.mainmenu' )
.requires( 'impact.image','game.entities.controllers.menu-control','game.entities.pointer' )
.defines(function(){
LevelMainmenu=/*JSON[*/{"entities":[{"type":"EntityMenuControl","x":0,"y":0},{"type":"EntityPointer","x":52,"y":28}],"layer":[]}/*]JSON*/;
});