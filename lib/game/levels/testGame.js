ig.module( 'game.levels.testGame' )
.requires( 'impact.image','game.entities.controllers.game-control','game.entities.objects.test-wall','game.entities.objects.test-border','game.entities.pointer' )
.defines(function(){
LevelTestGame=/*JSON[*/{"entities":[{"type":"EntityGameControl","x":0,"y":0},{"type":"EntityTestWall","x":84,"y":128},{"type":"EntityTestBorder","x":0,"y":-24},{"type":"EntityPointer","x":-116,"y":-152}],"layer":[]}/*]JSON*/;
});
