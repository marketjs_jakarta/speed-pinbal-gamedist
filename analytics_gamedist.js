(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

// PLEASE SUBSTITUTE THE UA-XXXXXXXX-YY with our own MarketJS Analytics. Each client is assigned one tracking code
// If no data shows, please consult the Google Analytics Chrome Debugger. Check if data is actually sent
// If working debugger should show: https://www.dropbox.com/s/5myrhw1flb4cu6c/Screen%20Shot%202014-10-30%20at%208.37.52%20PM.jpg?dl=0
ga('create', 'UA-102691241-1', {'name':'marketjs'}); // tracker name important
ga('marketjs.send', 'pageview'); // Send page view for tracker
