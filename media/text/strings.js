var _STRINGS = {
	"Ad":{
		"Mobile":{
			"Preroll":{
				"ReadyIn":"The game is ready in ",
				"Loading":"Your game is loading...",
				"Close":"Close",
			},
			"Header":{
				"ReadyIn":"The game is ready in ",
				"Loading":"Your game is loading...",
				"Close":"Close",
			},
			"End":{
				"ReadyIn":"Advertisement ends in ",
				"Loading":"Please wait ...",
				"Close":"Close",
			},
		},
	},

	"Splash":{
		"Loading":"Loading ...",
		"LogoLine1":"Some text here",
		"LogoLine2":"powered by MarketJS",
		"LogoLine3":"none",
	},

	"Game":{
		"SelectPlayer":"Select Player",
		"Win":"You win!",
		"Lose":"You lose!",
		"Score":"Score",
		"Time":"Time",

		"GemGet":"+1",
		"SureShotReady":"Sure Shot Ready!",
		"SureShotUsed":"SURE SHOT!",

		"Score":"SCORE",
		"Best":"BEST"
	},

	"Titles":{
		"Settings":"SETTINGS",
		"Upgrades":"UPGRADES",
		"GameOver":"GAME OVER",
		"Pause":"PAUSE",
		"Tutorial?":"TUTORIAL?",
	},

	"Upgrades":{
		"u_ballCount":"Extra Ball",
		"u_countdown":"Countdown Duration",
		"u_sureShot":"Sure Shot Interval",
		"u_magnet":"Magnet Duration",
		"u_spring":"Spring"
	},

	"Announcer":{
		"ballHit1":"NICE SHOT!",
		"ballHit2":"GREAT SHOT!",
		"ballHit3":"AWESOME!",
		"sureShotHit":"SURE SHOT!",
		"sureShotReady":"SURE SHOT READY!",
		"extra ball":"EXTRA BALL!",

		"bonus ball":"BONUS BALL!",
		"bonus gem":"BONUS GEM!",

		"magnet":"MAGNET!",
		"magnet over":"MAGNET OVER!",

		"spring hit":"SPRING!",
		"spring dead":"OUT OF SPRINGS!",

		"go":"GO!",

		"time over":"TIME'S UP!",
		// Changed from TIME OVER
		"out of balls":"OUT OF BALLS!"
	},

	"Tutorial":{
		"tut1_1mob":"Welcome to Speed Pinball!",
		"tut1_2mob":"Touch screen to continue...",
		"tut1_1desk":"Welcome to Speed Pinball!",
		"tut1_2desk":"Click to continue...",

		"tut2_1mob":"Touch the screen to use a flipper.",
		"tut2_2mob":"",
		"tut2_1desk":"Press the ← or → key to use each flipper.",
		"tut2_2desk":"",

		"tut3_1":"Hit the gems with the ball.",
		"tut3_2":"",

		"tut4_1":"Collect as many gems as you can before",
		"tut4_2":"timer bar runs out.",

		"tut5_1":"Get the magnet to collect nearby gems",
		"tut5_2":"automatically.",

		"tut6_1":"Once the sure-shot bar is full,",
		"tut6_2":"the ball will always hit a gem.",

		"tut7_1":"Buy upgrades at the home screen",
		"tut7_2":"to improve your game.",

		"tut8_1mob":"You are now ready!",
		"tut8_2mob":"Touch screen to continue...",
		"tut8_1desk":"You are now ready!",
		"tut8_2desk":"Click to continue...",
	},

	"Results":{
		"Title":"High score",
	},
};
